<?php

function formatStr($s, $tipo, $lung){
 switch ($tipo){
  case "N":
   $s = "000000000000000000000000000000000000000000000000000000000000000000000000000000000000".$s;
   $s = substr($s, -1*$lung);
   return $s;
   break;
 }
}

function CLEAN_VAR($buff){
        $buff=str_replace("-", "", $buff);
        $buff=str_replace(",", "", $buff);
        $buff=str_replace("(", "", $buff);
        $buff=str_replace(")", "", $buff);
        $buff=str_replace("'", "", $buff);
        $buff=str_replace(chr(34), "", $buff);
        $buff=str_replace("+", "", $buff);
        $buff=str_replace("/", "", $buff);
        $buff=str_replace("*", "", $buff);
        return $buff;
}


if (isset($_GET["progressivo"])){$progressivo=CLEAN_VAR($_GET["progressivo"]);}
if (isset($_GET["sequenza"])){$sequenza=CLEAN_VAR($_GET["sequenza"]);}
if (isset($_GET["id_foto"])){$id_foto=CLEAN_VAR($_GET["id_foto"]);}
if (isset($_GET["azienda"])){$azienda=strtolower(CLEAN_VAR($_GET["azienda"]));}
if (isset($_GET["sede"])){$sede=strtolower(CLEAN_VAR($_GET["sede"]));}

// Formulazione della cartella foto
// ---------------------------------------------------------------------------------------------------------------
$percorso="../GEA_4/foto/".$azienda."_".$sede."/";
if(!is_dir($percorso)) {mkdir($percorso, 0777);}
$percorso.="lav_".formatStr($progressivo, "N", 6)."/";
if(!is_dir($percorso)) {mkdir($percorso , 0777);}
$progressivo=formatStr($progressivo, "N", 6);
$sequenza=formatStr($sequenza, "N", 6);
for ($i=1; $i<=4; $i++){
        $cartella=substr($sequenza, $i-1, 1).substr("000000", 0, 6-$i);
        $percorso.=$cartella."/";
        if(!is_dir($percorso)) {mkdir($percorso , 0777);}
}
$pattern=$progressivo."_".$sequenza."_".$id_foto."_";
$dir = dir($percorso);
$memdataora="";
$dataora="";
$memnomefile="";
$nomefile = "";
while (($file = $dir->read()) !== false){
  if ($file == '..' || $file == '.' ){}else{
                $v=explode("_", $file);
        $ora=substr("0".$v[4], -6);
            $dataora=$v[3].$ora;
        if(substr($file, 0, strlen($pattern))==$pattern){
                $nomefile=$file;
                if (strlen($memdataora)>0){
                        if (floatval($dataora)<floatval($memdataora)){
                                $nomefile=$memnomefile;
                        } else {
                                $memnomefile=$nomefile;
                                $memdataora=$dataora;
                        }
                } else {
                        $memdataora=$v[3].$v[4];
                        $memnomefile=$nomefile;
                }
        }
  }
}

if (file_exists($percorso.$nomefile) && strlen($nomefile)>0){
        list($width, $height) = getimagesize($percorso.$nomefile);
        $proporzione=$height/$width;
        $direzione="orr";
        if($proporzione>1){$direzione="ver";}
        //copy($percorso.$nomefile, "small_$id_foto.jpg");
        //copy($percorso.$nomefile, "zoom_$id_foto.jpg");
        //resize($percorso, $nomefile, $id_foto);
        echo"1|$percorso$nomefile|$width|$height|$direzione|$proporzione";
} else {
        echo"0||0|0|0|0";
}

function resize($percorso, $nomefile, $id_foto){
$vdir_upload = "/";
list($width_orig, $height_orig) = getimagesize($percorso.$nomefile);
$dst_width = $width_orig;
$dst_height = $height_orig;
$im = imagecreatetruecolor($dst_width,$dst_height);
$image = imagecreatefromjpeg($percorso.$nomefile);
imagecopyresampled($im, $image, 0, 0, 0, 0, $dst_width, $dst_height, $width_orig, $height_orig);
imagejpeg($im,"zoom_$id_foto.jpg");
imagedestroy($im);
}




?>