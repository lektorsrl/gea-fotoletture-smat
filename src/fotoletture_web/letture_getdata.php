<?php

session_start();
require_once("funzioni.php");

Apri_DB("sede");

$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 14;
$caricofile_id = isset($_POST['caricofile_id']) ? mysql_real_escape_string($_POST['caricofile_id']) : '';

$pdr = isset($_POST['pdr']) ? mysql_real_escape_string($_POST['pdr']) : '';
$codice_utente = isset($_POST['codice_utente']) ? mysql_real_escape_string($_POST['codice_utente']) : '';
$utente = isset($_POST['utente']) ? mysql_real_escape_string($_POST['utente']) : '';
$matricola = isset($_POST['matricola']) ? mysql_real_escape_string($_POST['matricola']) : '';
$segnalazione = isset($_POST['segnalazione']) ? mysql_real_escape_string($_POST['segnalazione']) : '';
$acc = isset($_POST['acc']) ? mysql_real_escape_string($_POST['acc']) : '';
$flag = isset($_POST['flag']) ? mysql_real_escape_string($_POST['flag']) : '';
$caricofile_id= isset($_POST['caricofile_id']) ? mysql_real_escape_string($_POST['caricofile_id']) : '';

$query_ente_file=$_SESSION["ses-query_ente_file"];
$query_ente_sede=$_SESSION["ses-query_ente_sede"];

$offset = ($page-1)*$rows;
$result = array();

// ______________________________________________________________________________________________ QUERY WHERE

$queryWhere="where 1 $query_ente_sede";
if (strlen($caricofile_id)>0){$queryWhere.=" and indice_file=$caricofile_id ";}
if (strlen($pdr)>0){$queryWhere.=" and matricola_contatore_padre=$pdr ";}
if (strlen($codice_utente)>0){$queryWhere.=" and codice_utente=$codice_utente ";}
if (strlen($utente)>0){$queryWhere.=" and utente like '%$utente%' ";}
if (strlen($matricola)>0){$queryWhere.=" and matricola like '%$matricola%' ";}
if (strlen($segnalazione)>0){$queryWhere.=" and segn1='$segnalazione' ";}
if (strlen($acc)>0){$queryWhere.=" and acc='$acc' ";}
if (strlen($flag)>0){$queryWhere.=" and flag='$flag' ";}

// __________________________________________________________________________________________________________


$QUERY_CONTA="select count(*) from letture $queryWhere";

$rs = mysql_query($QUERY_CONTA);
$row = mysql_fetch_row($rs);
$result["total"] = $row[0];

//$myfile = fopen("logs.txt", "a") or die("Unable to open file!");
//fwrite($myfile, "\n"."[$caricofile_id]");
//fclose($myfile);

$QUERY_FILE= "select * from
                      letture inner join caricofile on letture.indice_file=caricofile.caricofile_id
                      $queryWhere
                      order by progressivo desc
                      limit $offset,$rows
                      ";

//die($QUERY_FILE);
//writelog($QUERY_FILE."\n");

$rs = mysql_query($QUERY_FILE);

$rows = array();

$percorsosede="../GEA_4/foto/".$_SESSION["ses-ut-ditta"]."_".$_SESSION["ses-ut-sede"]."/";

while($row = mysql_fetch_object($rs)){

    foreach($row as $key => $value) {
        if (!is_null($value)) {
            $row->$key = utf8_encode($value);

        }
    }

    $progressivo=formatStr($row->progressivo, "N", 6);
    $sequenza=formatStr($row->sequenza, "N", 6);

    $percorso = $percorsosede."lav_".formatStr($progressivo, "N", 6)."/";

    for ($i=1; $i<=4; $i++){
        $cartella=substr($sequenza, $i-1, 1).substr("000000", 0, 6-$i);
        $percorso.=$cartella."/";
    }
    $row->foto = scanForPhotos($percorso, $progressivo."_".$sequenza);

    array_push($rows, $row);
}
$result["rows"] = $rows;

echo json_encode($result);



function scanForPhotos($basePath, $comparisonString) {
    $result = [];
    if (is_dir($basePath)) {
        foreach (scandir($basePath) as $file) {
            $buff = substr($file,0,strlen($comparisonString));
            if (strncmp($buff, $comparisonString, strlen($comparisonString)) === 0) {
                $result[] = $basePath."/".$file;
            }
        }
    }

    return $result;
}

function WriteLog($s){
    $myfile = fopen("logs_foto.txt", "a");
    fwrite($myfile, $s);
    fclose($myfile);
}

?>