<?php

require_once("pclzip.lib.php");


if (!isset($_SESSION["connesso"])){
  
  $_SESSION["connesso"]="no";
  
  
  // Utilizzatore
  $_SESSION["ses-ut-nome"]="";
 	$_SESSION["ses-ud-id"]=0;
 	$_SESSION["ses-ut-ruolo"]=20;
 	$_SESSION["ses-ut-sede"]=1;
 	$_SESSION["ses-ut-ente"]=17;
 	$_SESSION["ses-connesso"]="no";
 	$_SESSION["ses-ut-sede-db"]= "???";
	$_SESSION["ses-ut-ditta"]= "";
	$_SESSION["ses-ut-sede"]= "???"; 
	$_SESSION["ses-ut-enti_per_query"]= ""; 
	
	$_SESSION["ses_letturisti"]="";
	
	
 	// Query Ente-Sede
 	$_SESSION["ses-query_ente_sede"]="";
 	$_SESSION["ses-query_ente_file"];
 	
  
  // Percorsi
  $_SESSION["ses-dirfoto"]="foto/";
  
  // Gestione Pagine
  $_SESSION["ses-pagina"]=1;
  $_SESSION["ses-righe_per_pagina"]=30;
  $_SESSION["ses-tot_pagine"]=0;
  $_SESSION["ses-sequenza"]="1";
  
  
  // Trasporto query tra pagine
  $_SESSION["ses-query"]="";
  
  // georeferenziazione
  $_SESSION["ses-tipomappa"]="";
  $_SESSION["ses-numpunti"]=1;
  $_SESSION["ses-latitudini"]="";
  $_SESSION["ses-longitudini"]="";
  $_SESSION["ses-nomi"]="";
  $_SESSION["ses-indirizzi"]="";
  $_SESSION["ses-tipi"]="";
  $_SESSION["ses-zoom"]="";
  $_SESSION["ses-larghezza"]="";
  $_SESSION["ses-altezza"]="";
  $_SESSION["ses-controlli"]="";
	$_SESSION["ses-comune"]="";
	$_SESSION["ses-file"]="";
	
	// Altro
	$_SESSION["ses-mostra_download_foto"]=1;

}

require_once("config/config.php");
 
 
// Apro il db System e vedo se c'� almeno una lavorazione
//------------------------------------------------------------------------------------------------------------------------------------------- 
Apri_DB("system");

//Controllo che ci sia almeno una lavorazione altrimento blocco tutto
$query="select * from lavorazione";
$ris = mysql_query($query);
if (mysql_num_rows($ris)==0){
	include("no_dati.php");
	die("");
}


$ris=mysql_query("select * from letturista", $connessione);
$buff="";
while ($rs=mysql_fetch_array($ris)){
	$i=$rs["letturista_numero"];
	$Letturisti[$i]=$rs["letturista_nome"];
}
//------------------------------------------------------------------------------------------------------------------------------------------- 


// Apro il DB Fotoletture
//------------------------------------------------------------------------------------------------------------------------------------------- 
Apri_DB("fotoletture");
//------------------------------------------------------------------------------------------------------------------------------------------- 


if(!function_exists('IsDateDodo')) {
	function IsDateDodo($stringa) {
	  
	  $d = substr($stringa, 0, 2);
	  $m = substr($stringa, 3, 2);
	  $y = substr($stringa, 6, 4);
	  return checkdate ($m, $d, $y);
	}
}	

function IsDate($string) {
  $t = strtotime($string);
  $m = date('m',$t);
  $d = date('d',$t);
  $y = date('Y',$t);
  //DEBUG ("time",$t);
  //DEBUG ("mese",$m);
  //DEBUG ("giorno",$d);
  //DEBUG ("anno",$y);
  return checkdate ($m, $d, $y);
}

function FormatStr($s, $tipo, $lung){
 $stringazza="                                                                                                                                                                ";
 $numerazzo="00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
 $risultato=$s;
 switch ($tipo){
  case "SD": $risultato = substr($s.$stringazza, 0, $lung); break;
  case "SS": $risultato = substr($stringazza.$s, (-1)*$lung); break;
  case "N":  $risultato = substr($numerazzo.$s, (-1)*$lung); break;
 }
 return $risultato;
}

function fill_cella($tipo, $campo, $align, $classe){
global $riga;
 echo'<td class="'.$classe.'" align="'.$align.'">';
 switch ($tipo){
  case 1:
   echo strtoupper($campo);
   break;
  case 2:
  
   if ($_SESSION["ut-ente"]==2227 && $campo=="pdr"){
   	$buff=substr($riga["extra"], 20, 14); 
   } else {
  	$buff=$riga["$campo"];
   }
   
   
   if (($campo=="segn1" || $campo=="segn2" || $campo="segn3")&& $buff=="0"){$buff="";}
   if ($campo=="letlocalita"){if (strlen($buff)==0){$buff=$riga["letcomune"];}} 
   if(strlen($buff)>0) echo strtoupper($buff); else echo '&nbsp;';
   break; 
 } 
 echo'</td>';
}


function data_diff($interval, $date1, $date2) { 
// date1 � la pi� vecchia
// date2 � la pi� giovane
    $seconds = $date2 - $date1; 
    switch ($interval) { 
        case "y":    // years 
            list($year1, $month1, $day1) = split('-', date('Y-m-d', $date1)); 
            list($year2, $month2, $day2) = split('-', date('Y-m-d', $date2)); 
            $time1 = (date('H',$date1)*3600) + (date('i',$date1)*60) + (date('s',$date1)); 
            $time2 = (date('H',$date2)*3600) + (date('i',$date2)*60) + (date('s',$date2)); 
            $diff = $year2 - $year1; 
            if($month1 > $month2) { 
                $diff -= 1; 
            } elseif($month1 == $month2) { 
                if($day1 > $day2) { 
                    $diff -= 1; 
                } elseif($day1 == $day2) { 
                    if($time1 > $time2) { 
                        $diff -= 1; 
                    } 
                } 
            } 
            break; 
        case "m":    // months 
            list($year1, $month1, $day1) = split('-', date('Y-m-d', $date1)); 
            list($year2, $month2, $day2) = split('-', date('Y-m-d', $date2)); 
            $time1 = (date('H',$date1)*3600) + (date('i',$date1)*60) + (date('s',$date1)); 
            $time2 = (date('H',$date2)*3600) + (date('i',$date2)*60) + (date('s',$date2)); 
            $diff = ($year2 * 12 + $month2) - ($year1 * 12 + $month1); 
            if($day1 > $day2) { 
                $diff -= 1; 
            } elseif($day1 == $day2) { 
                if($time1 > $time2) { 
                    $diff -= 1; 
                } 
            } 
            break; 
       case "w":    // weeks 
            // Only simple seconds calculation needed from here on 
            $diff = floor($seconds / 604800); 
            break; 
       case "d":    // days 
            $diff = floor($seconds / 86400); 
            break; 
       case "h":    // hours 
            $diff = floor($seconds / 3600); 
            break; 
       case "i":    // minutes 
            $diff = floor($seconds / 60); 
            break; 
       case "s":    // seconds 
            $diff = $seconds; 
            break; 
    } 
    return $diff; 
}

function flash($width, $height, $nome){
 echo'
  <OBJECT classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" WIDTH="'.$width.'" HEIGHT="'.$height.'" id="'.$nome.'" ALIGN="">
   <PARAM NAME=movie VALUE="'.$nome.'"> 
   <PARAM NAME=quality VALUE=high> 
   <PARAM NAME=bgcolor VALUE=#FFFFFF> 
   <EMBED src="'.$nome.'" quality=high bgcolor=#FFFFFF  WIDTH="'.$width.'" HEIGHT="'.$height.'" NAME="'.$nome.'" ALIGN="" TYPE="application/x-shockwave-flash" PLUGINSPAGE="http://www.macromedia.com/go/getflashplayer"></EMBED>
  </OBJECT>
 ';
}



function StripTrim($s){
	return trim(str_replace("'","''",stripslashes($s)));
}


function MODULO($nome, $cosa, $etichetta='', $oggetto='', $tipo='', $valore=''){
global $connessione; 
global $filtri;
global $id_utente;

	 switch ($cosa){
	 	case "vuoto":
	 	 echo'<TR><TD HEIGHT="'.$etichetta.'" COLSPAN=2>&nbsp</TD></TR>';
	 	 break;
	 	 
	 	case "linea":
	 	 echo'<TR><TD HEIGHT="'.$etichetta.'" COLSPAN=2><hr color="#F0F0F0"></TD></TR>';
	 	 break; 
	 	 
	 	case "apri_field":
	 	  echo'<fieldset ><br>
  	       <TABLE  CELLPADDING="0" HEIGHT="'.$oggetto.'">';
	 	 break; 
	 	 
	 	case "chiudi_field":
	 	  echo'</TABLE></fieldset>'; 
	 	  break; 
	 	  
	  case "apri":
	    echo'
	     <FORM NAME="'.$nome.'" ACTION="'.$_SERVER["PHP_SELF"].'" METHOD="POST" TARGET="_self">
	      <input type="hidden" name="fase" value="'.$etichetta.'"> 
	      <fieldset ><br>
  	      <TABLE  CELLPADDING="0" HEIGHT="'.$oggetto.'" border="0">
	    '; 
	   break;
	   
    case "chiudi": 
	    echo'</TABLE></fieldset>'; 
	   break; 
	   
  	case "submit":
      $stile="bottone_quadrato";  
      if (strlen(trim($filtri))>0){$stile="bottone_quadrato_on";} 
	    echo'
	      <TD>
	      </TD>
	      <TD>
	       <TABLE CELLPADDING="0" CELLSPACING="0" BORDER="0">
	        <TR>
	         <TD><input type="submit" class="'.$stile.'" value="'.$etichetta.'"></TD></FORM>';
	         if ($oggetto=="XXX"){
	         	echo '
	         	 <FORM NAME="Annulla" ACTION="'.$_SERVER["PHP_SELF"].'" METHOD="POST" TARGET="_self">
	         	  <input type="hidden" name="fase" value="'.$tipo.'"> 
	         	  <TD>
	         	   &nbsp;<input type="submit" class="bottone_quadrato" value="Annulla">
	         	  </TD>
	         	 </FORM> 
	         	';  
	         }
	        echo' 
	        </TR>
	       </TABLE>  
	      </TD>
        ';
	    break; 
	      
    case "campo":
         $stilelabel="form_label";  
         if (strlen(trim($valore))>0){$stilelabel="form_label_on";}
			   switch ($tipo){
			    case "TXT":
			     echo'<TR><TD CLASS="'.$stilelabel.'" align="RIGHT">'.$etichetta.':</TD><TD ><INPUT CLASS="form_campo" TYPE="text" NAME="'.$oggetto.'" VALUE="'.$valore.'"></TD></TR>';
			     break;
			    case "PWD":
			     echo'<TR><TD CLASS="'.$stilelabel.'" align="RIGHT">'.$etichetta.':</TD><TD ><INPUT CLASS="form_campo" TYPE="password" NAME="'.$oggetto.'" VALUE="'.$valore.'"></TD></TR>';
			     break; 
			    case "LOT":
			     $query="select * from lotti order by id desc";
			     $ris_lot = mysql_query($query, $connessione);
			     echo'<TR><TD CLASS="'.$stilelabel.'" align="RIGHT">'.$etichetta.':</TD>
			           <TD CLASS="'.$stilelabel.'" align="RIGHT">
			            <SELECT ID="'.$oggetto.'" NAME="'.$oggetto.'" class="selectmisura"> 
					         <option value="">Lotto in corso</option>';
						        while ($rs=mysql_fetch_array($ris_lot)){
						         echo'<option value="'.$rs["prefisso"].'" ';
						         if ($valore==$rs["prefisso"]) {echo "selected";}
						         echo'>'.$rs["nome"].'</option>';
						        } // while
					        echo'
					        </SELECT>
					       </TD>
					      </TR>';
			     break; 
			   case "CHECK":
			     $classecheck="form_campo_check";
			     if ($valore=="TRUE"){$classecheck="form_campo_checked";}
			     echo'<TR><TD ALIGN="LEFT" COLSPAN=2><INPUT CLASS="'.$classecheck.'" TYPE="CHECKBOX" NAME="'.$oggetto.'" VALUE="TRUE" '; if ($valore=="TRUE"){echo "checked";} echo' ><SPAN CLASS="'.$classecheck.'">'.$etichetta.'&nbsp;&nbsp;</SPAN></TD></TR>';
			     break; 
			   }     
	      break;
	 } // cosa
 
}// function


function MODULO_RIGA($nome, $cosa, $etichetta='', $oggetto='', $tipo='', $valore=''){
global $connessione; 

	 switch ($cosa){
	  case "apri":
	    echo'
	     <FORM NAME="'.$nome.'" ACTION="'.$_SERVER["PHP_SELF"].'" METHOD="POST" TARGET="_self">
	      <input type="hidden" name="fase" value="'.$etichetta.'"> 
  	      <TABLE  CELLPADDING="2" border="0"><TR>
	    '; 
	   break;
   case "chiudi": 
	    echo'</TR></TABLE>'; 
	   break; 
	case "hidden":
	 echo '<input type="hidden" name="'.$etichetta.'" value="'.$oggetto.'">';
	 break;   
	case "submit":
	    echo'
	      <TD>
	       <TABLE CELLPADDING="0" CELLSPACING="0" BORDER="0" ALIGN="LEFT">
	        <TR>
	         <TD><input type="submit" class="bottone" value="'.$etichetta.'">
	         </TD>
	         </FORM>
	        </TR>
	       </TABLE>  
	      </TD>
	     ';
	   break;   
  case "campo":
			   switch ($tipo){
			    case "TXT":
			     $classe="form_campo";
			     $classe_label="form_label";
			     if ($valore=="###ERRORE###"){
			     	 $valore = "";
			     	 $classe="form_campo_err";
			     	 $classe_label="form_label_err";
			     }
			     echo'<TD CLASS="'.$classe_label.'" align="RIGHT">'.$etichetta.':</TD><TD ><INPUT CLASS="'.$classe.'" TYPE="text" NAME="'.$oggetto.'" VALUE="'.$valore.'"></TD>';
			     break;
			    case "PWD":
			     echo'<TD CLASS="form_label" align="RIGHT">'.$etichetta.':</TD><TD ><INPUT CLASS="form_campo" TYPE="password" NAME="'.$oggetto.'" VALUE="'.$valore.'"></TD>';
			     break; 
			    case "CHECK":
			     echo'<TD CLASS="form_label" align="RIGHT"></TD><TD ALIGN="LEFT"><INPUT CLASS="form_campo_check" TYPE="CHECKBOX" NAME="'.$oggetto.'" VALUE="TRUE" '; if ($valore=="TRUE"){echo "checked";} echo' >'.$etichetta.'</TD>';
			     break;  
			    case "COMBO":
					     echo'<TD CLASS="form_label" align="RIGHT">'.$etichetta.':</TD>';
					     estraiRecordSet($oggetto, $ris_form);
					     echo'
					       <TD>
					        <SELECT ID="'.$oggetto.'" NAME="'.$oggetto.'" class="SELECT"> 
					         <option value=""></option>';
					        while ($rs=mysql_fetch_array($ris_form)){
					         echo'<option value="'.$rs["id"].'" ';
					         if ($valore==$rs["id"]) {echo "selected";}
					         echo'>'.$rs["nome"].'</option>';
					        } // while
					        echo'
					        </SELECT>
					       </TD>';
					       
			     break; 
			    } // switch tipo
	   break;
	   
	 } // cosa
 
}

function creaFileFoto($dirfoto, $dir_tmp_foto, $lavorazione){
global $dirfoto;
global $dir_tmp_foto;

 Apri_DB("system");
 $query = "select * from lavorazione where lavorazione_progressivo=$lavorazione";
 $ris=mysql_query($query);
 $rs = mysql_fetch_array($ris);
 $nomelavorazione = strtoupper($rs["lavorazione_nome"]);
 $ripassi=0;
 $cartoline=0;

 if (substr_count($nomelavorazione, "RIPASSI")>0){$ripassi=1;}
 if (substr_count($nomelavorazione, "CARTOLINE")>0){$cartoline=1;} 
 
 Apri_DB("fotoletture");
 mkdir($dir_tmp_foto);
 if (is_dir($dirfoto)){
  $dir = dir($dirfoto);
  while (($file = $dir->read()) !== false){
   if ($file!="." && $file!=".."){
    $s=substr($file, 3);
    $lavorazione=substr($s, 0, strpos($s, "_"));
    $s=substr($s, strpos($s, "_")+1);
    $sequenza=substr($s, 0, strpos($s, "_"));
    $s=substr($s, strpos($s, "_")+1);
    $progfoto=substr($s, 0, strpos($s, "_"));
    if (is_numeric($progfoto)){$progfoto=Formatstr($progfoto, "N", 2);}
    if ($progfoto!="01" && $progfoto !="02" && $progfoto !="03"){
     $progfoto="01";
    }
    
    
    $nome_Tabella = "lav_".formatstr($lavorazione, "N", 6);
    $query="select * from $nome_Tabella where letsequenza=$sequenza";
    //echo ($query);
    $ris=mysql_query($query);
    if (mysql_num_rows($ris)){
     $rs = mysql_fetch_array($ris);
     $posizione=formatstr($rs["letposizione"], "N", 10);
     $periodo_fatturazione=substr($rs["letpin"], 0, 6);
     $periodo_fatturazione=substr($periodo_fatturazione, 5, 1).substr($periodo_fatturazione, 0, 4);
     
     if ($progfoto=="01" && $ripassi==1){$progfoto="R1";}
     if ($progfoto=="02" && $ripassi==1){$progfoto="R2";}
     if ($progfoto=="03" && $ripassi==1){$progfoto="R3";}
     
     if ($progfoto=="01" && $cartoline==1){$progfoto="C1";}
     if ($progfoto=="02" && $cartoline==1){$progfoto="C2";}
     if ($progfoto=="03" && $cartoline==1){$progfoto="C3";}
     
    $new_nome_foto=$posizione.$periodo_fatturazione.$progfoto.".jpg";
     copy($dirfoto.$file, $dir_tmp_foto.'/'.$new_nome_foto);
    } 
    
    //echo "File: $file <br>";
    //echo "Lav: $lavorazione<br>";
    //echo "Seq: $sequenza <br>";
    //echo "Prog: $progfoto <br>";
    //echo "<hr><br>";
   }
  } 
 }
 

}


function creaZipLetture(){
global $file_id;

 

 // Con File_id recupero nome del file e progressivo lavorazione
 $query="select * from caricofile where caricofile_id=$file_id";
 $ris=mysql_query($query) or die("Errore selezione file da spedire file_id=[$file_id]: " . mysql_error());
 $rs=mysql_fetch_array($ris);
 $progressivo=$rs["caricofile_progressivo"];
 $buff=explode(".",$rs["caricofile_nome"]);
 $nomeFile=$buff[0];
 
 // Genero cartella, nome file OUT e nomeZip
 $dirZip="zip/txt/";
 $d = dir($dirZip);
 while (false !== ($nomeZip = $d->read())) {if ($nomeZip!="." && $nomeZip!=".."){unlink($dirZip.$nomeZip);}}
 $nomeZip=date("Ymd")."_".date("His")."_".$nomeFile.".zip";
 $nomeFile.=".OUT";

 Apri_DB("system");
 $query = "select * from lavorazione where lavorazione_progressivo=$progressivo";
 $ris=mysql_query($query)or die("Errore apertura lavorazione [$lavorazione]: " . mysql_error());
 $rs = mysql_fetch_array($ris);
 $nomelavorazione = strtoupper($rs["lavorazione_nome"]);
 $ripassi=0;
 $cartoline=0;
 if (substr_count($nomelavorazione, "RIPASSI")>0){$ripassi=1;}
 if (substr_count($nomelavorazione, "CARTOLINE")>0){$cartoline=1;} 
 
 Apri_DB("sede");
 
 // Memorizzo i Fabbricanti per dopo
 $query="select * from cataloghi";
 $ris=mysql_query($query) or die("Errore select cataloghi per spedizione la lavorazione=[$progressivo] ed il file file_id=[$file_id]: " . mysql_error());
 $buff="";
 while ($rs=mysql_fetch_array($ris)){
 	 $buff.="[".$rs["id"]."]".$rs["descrizione"]."|";
 }
 $FABBRICANTI=explode("|", $buff);
 
 $query="select * from letture where progressivo=$progressivo and indice_file=$file_id and stato='SPE' order by codice_utente asc";
 $ris=mysql_query($query) or die("Errore select letture da spedire per la lavorazione=[$progressivo] ed il file file_id=[$file_id]: " . mysql_error());
 //die($query);
 
 $FILE_LETTURE = fopen($dirZip.$nomeFile, 'w') or die("Errore apertura file [".$dirZip.$nomeFile."]");
 
 while ($rs=mysql_fetch_array($ris)){
 	
 	 
 	 $data_lettura=$rs["data_lettura"];
 	 if (strlen($data_lettura)==10){$data_lettura = substr($data_lettura, -4).substr($data_lettura, 3, 2).substr($data_lettura, 0, 2);}else{$data_lettura=date("Ymd");}
 	 $ora_lettura=substr("00000000".$rs["ora_lettura"], -8);
 	 $ora_lettura=substr($ora_lettura, 0, 2).substr($ora_lettura, 3, 2);
 	 $extra=$rs["extra"];
 	 
 	 
 	 $lettura=$rs["lettura"];
 	 $lettura_precedente_ente=$rs["lettura_precedente_ente"];
 	 $lettura= str_replace(".", "", $lettura);
 	 $segn1=trim($rs["segn1"]);
 	 if($segn1=="0 "||$segn1==" 0"||$segn1=="0"){$segn1=" ";}
 	 $matricola_nuova=trim($rs["matricola_nuova"]);
 	 $matricola=trim($rs["matricola"]);
 	 $nota=trim($rs["nota"]);
 	 $cifre=trim($rs["cifre"]);
 	 $cifre_nuove=trim($rs["cifre_nuove"]);
 	 $pdr=$rs["pdr"];
 	 
 	 // ___________________________________________________________________ Flag Mancata Lettura
 	 $mancata_lettura="  ";
 	 if (trim($lettura)==""){$mancata_lettura="ML";}
 	 
 	 // ___________________________________________________________________ Cambio Contatore
 	 $segn_cambio_cont="  ";
 	 $cambio_cont=" ";
 	 $lettura_cambio_cont="0000000";
 	 $data_cambio_cont="        ";
 	 $matricola_nuova="         ";
 	 if (trim($segn1)=="C"){
 	 	$segn1="  "; 
 	 	$cambio_cont="1";
 	 	
 	 	$lettura_cambio_cont=FormatStr($lettura, "N", 7);;
 	 	$data_cambio_cont=FormatStr($data_lettura, "SD", 8);
 	 	$matricola_nuova=FormatStr($rs["matricola_nuova"], "SD", 9);
 	 	
 	 	// Nuove specifiche 18/07/2014. Secondo istruzioni di Cerri nei cambi contatore mando data e lettura come ricevute da SMAT
    // InserisciNuovoValoreInStringaOut "lettura", Space(7), s
    //$lettura="       ";
    if (strlen($extra)==92){$lettura=substr($extra, 85, 7);}else{$lettura=substr($extra, 156, 7);}
    $data_lettura= substr($extra, 14, 8);
 	 }
 	 
 	 
 	 // ___________________________________________________________________ Matricola Errata
 	 if (trim($segn1)=="M"){
     if (strlen(trim($matricola_nuova))>0){$nota = "NEW MAT[".$matricola_nuova."] ".$nota;} else {$segn1="  ";}
   }
 	 
 	 // ___________________________________________________________________ Fabbricante
 	 $cod_fabbricante=$rs["fabbricante"];
 	 $cod_fabbricante_nuovo=$rs["fabbricante_nuovo"];
 	 $fabbricante="   ";
 	 foreach ($FABBRICANTI as $buff){
 	 	 $pos = strpos($buff, $cod_fabbricante);
		 if (!($pos === false)) {
		 	  $buff2=explode("]", $buff);
		 	  $fabbricante=FormatStr($buff2[1], "SD", 3);
		 	  //echo("PDR=".$pdr."<br>Cod Fabbricante= ".$cod_fabbricante."<br>Cod Fabbricante Nuovo=".$cod_fabbricante_nuovo."<br>FABBRICANTI= ".$FABBRICANTI."<br>Pos= ".$pos."<br>Buff= ".$buff."<br>Buff2= ".$buff2."<br>Fabbricante=".$fabbricante);
		 	  break;
			}
 	 }
 	
 	 // ___________________________________________________________________ Stato Contatore
 	 $stato_cont=$rs["stato_contatore"];
 	 switch ($stato_cont){
 	 	 case "A": $stato_cont="ATT"; break;
 	 	 case "R": $stato_cont="RIM"; break;
 	 	 default: $stato_cont="   "; break;
 	 }
 	 
 	 // ___________________________________________________________________ Flag Forzatura
 	 $flag=$rs["flag"];
 	 
 	 if ($flag=="I"){ if ($lettura-$lettura_precedente_ente>0){$flag="L";} }  
 	   
 	 
 	 
 	 $lettura_forzata="0";
 	 $pos = strpos("|A|B|N|I|G|", "|".$flag."|");
 	 if (!($pos === false)){$lettura_forzata="1";}
 	 
 	 // ___________________________________________________________________ Nuove Note
 	 $flag_note_lett=" ";	 
 	 $note=$rs["note"];
 	 $note_nuove=trim($rs["note_nuove"]);
 	 if (strlen($note_nuove)==0){$note_nuove=trim($rs["ubicazione_nuova"]);}
 	 if (strlen($note_nuove)>0){
 	 	 if ($note!=$note_nuove) {
 	 	 	 $note= $note_nuove;
 	     $flag_note_lett="1";
 	   } else {
 	   	 $note=""; 
 	   }
 	 }  
 	 
 	 $data_primo_passaggio=$rs["data_primo_passaggio"];
 	 if (strlen($data_primo_passaggio)==10){$data_primo_passaggio = substr($data_primo_passaggio, -4).substr($data_primo_passaggio, 3, 2).substr($data_primo_passaggio, 0, 2);}else{$data_primo_passaggio="00000000";}
 	 $ora_primo_passaggio=$rs["ora_primo_passaggio"];
 	 if (strlen($ora_primo_passaggio)==8){$ora_primo_passaggio = substr($ora_primo_passaggio, 0, 2).substr($ora_primo_passaggio, 3, 2);}else{$ora_primo_passaggio="0000";}
 	 $data_secondo_passaggio=$rs["data_secondo_passaggio"];
 	 if (strlen($data_secondo_passaggio)==10){$data_secondo_passaggio = substr($data_secondo_passaggio, -4).substr($data_secondo_passaggio, 3, 2).substr($data_secondo_passaggio, 0, 2);}else{$data_secondo_passaggio="00000000";}
 	 $ora_secondo_passaggio=$rs["ora_secondo_passaggio"];
 	 if (strlen($ora_secondo_passaggio)==8){$ora_secondo_passaggio = substr($ora_secondo_passaggio, 0, 2).substr($ora_secondo_passaggio, 3, 2);}else{$ora_secondo_passaggio="0000";}
 	 
 	 $tentativi=$rs["tentativi"];
 	 if ($tentativi>3){$tentativi="1";}else{$tentativi=" ";}
 	 
 	 // ___________________________________________________________________ Coordinate
 	 $latitudine=str_replace(".", ",", $rs["latitudine"]);
 	 $longitudine=str_replace(".", ",", $rs["longitudine"]);
 	 if (strlen($latitudine)!="" && strlen($longitudine)!=""){
 	 	  $buff=explode(",", $latitudine);
 	    $latitudine= substr("0".$buff[0], -2).substr($buff[1]."0000000", 0, 7);
 	    $buff=explode(",", $longitudine);
 	    $longitudine= substr("0".$buff[0], -2).substr($buff[1]."0000000", 0, 7);
 	 } else {
	 	 $latitudine="         ";
	 	 $longitudine="         ";
 	 }
 	 
 	 $flag_georef="1";
 	 $flag_no_georef=" ";
 	 if (strlen(trim($latitudine))==0 || strlen(trim($longitudine))==0){$flag_no_georef="1";}
 	 
 	 // ___________________________________________________________________ Nuove cifre
 	 $buff=$cifre_nuove;
 	 $flag_cifre="0";
 	 if (strlen($buff)==0){$buff=$cifre;} 
 	 if ($cifre!=$cifre_nuove && is_numeric($cifre) && is_numeric($cifre_nuove)){$flag_cifre="1";}
 	 $cifre=$buff;
 	 
 	 // ___________________________________________________________________ Matricola nuova in caso di matricola assente SMAT
   $flag_mat_nuova="0";
   $matricola_nuova_vuota="         ";
   if (strlen(trim($matricola_nuova))>0 && strlen(trim($matricola))==0){
   	 $matricola_nuova_vuota=$matricola_nuova;
   	 $flag_mat_nuova="1";
   }	
   
   // ___________________________________________________________________ Cartolina
   $flag_cartolina=" ";
   if ($segn1=="A"){$flag_cartolina="1";}
   
   // ___________________________________________________________________ Consumi Negativi
   // 02/02/2015 W la metto sui consumi negativi basta che non sia un cambi contatore
   
   //if ($pdr=="10023489"){
   //	 die("Segn1=$segn1  Flag=$flag FlagMatNuova=$flag_mat_nuova  MatNuova=$matricola_nuova");
   //	}
   
   if (($flag=="I" || $flag=="G") && strlen(trim($matricola_nuova))==0){$segn1="W";}
	
   
   // ___________________________________________________________________ Date Ore primo passaggio
   If (strlen($data_primo_passaggio)==0 || $data_primo_passaggio=="00000000") {
     $data_primo_passaggio=$data_lettura;
     $ora_primo_passaggio=$ora_lettura;
   } elseif(strlen($data_secondo_passaggio)==0 || $data_secondo_passaggio=="00000000") {
     If ($data_primo_passaggio!=$data_lettura || $ora_primo_passaggio!=$ora_lettura){
       $data_secondo_passaggio = $data_lettura;
       $ora_secondo_passaggio = $ora_lettura;
     } Else {
       $data_secondo_passaggio = "";
       $ora_secondo_passaggio = "";
     }
   } else {
     $data_secondo_passaggio = $data_lettura;
     $ora_secondo_passaggio = $ora_lettura;
   }
   
   if ($data_primo_passaggio=="00000000"){$data_primo_passaggio=="        ";}
   if ($ora_primo_passaggio=="0000"){$data_primo_passaggio=="    ";}
   if ($data_secondo_passaggio=="00000000"){$data_secondo_passaggio="        ";}
   if ($ora_secondo_passaggio=="0000"){$ora_secondo_passaggio="    ";}
   
   switch (substr($nomeFile, 7, 1)){
   	
   	case "A":
   	
   	   $periodo_fatturazione=substr($rs["extra"], 16, 5);
   	   $periodo_fatturazione=substr($periodo_fatturazione, 0, 4)."0".substr($periodo_fatturazione, -1);
   	
	   	 $s=FormatStr($rs["codice_utente"], "N", 10);
		 	 $s.=FormatStr($rs["codice_utente"], "N", 10);
		 	 $s.=$periodo_fatturazione;
		 	 $s.="00000000";
		 	 $s.=FormatStr($data_lettura, "SD", 8);
		 	 $s.="0000000";
		 	 $s.=FormatStr($lettura, "N", 7);
		 	 $s.=FormatStr(" ", "SD", 1);
		 	 $s.=FormatStr($segn1, "SD", 2);
		 	 $s.="      00000000000000";
		 	 $s.=str_repeat(' ', 492);
		 	 break;
	
		 	 
	
		 	 
		default: 	 
		 	 if ($cartoline==1){
	// su segnalazione di Smat fatta ad Egometers e Implanet non mando indietro la nota letturista e le note contatore che erano sporcate da noi con progressivo-sequenza sulle cartoline scansionate per vedere se erano state gi� inserite
	
		 	 $s=FormatStr($rs["codice_utente"], "N", 10);
		 	 $s.=FormatStr($pdr, "N", 10);
		 	 $s.=FormatStr(substr($extra, 0, 14), "SD", 14);
		 	 $s.=FormatStr($data_lettura, "SD", 8);
		 	 $s.=FormatStr($rs["lettura_precedente_ente"], "N", 7);
		 	 $s.=FormatStr($lettura, "N", 7);
		 	 $s.=FormatStr(" ", "SD", 1);
		 	 $s.=FormatStr($segn1, "SD", 2);
		 	 $s.=FormatStr($mancata_lettura, "SD", 2);
		 	 $s.=FormatStr($segn_cambio_cont, "SD", 2);
		 	 $s.=FormatStr($cambio_cont, "SD", 1);
		 	 $s.=FormatStr(substr($extra, 23, 8), "SD", 8);
		 	 $s.=FormatStr($lettura_cambio_cont, "N", 7);
		 	 $s.=FormatStr($data_cambio_cont, "SD", 8);
		 	 $s.=FormatStr($matricola_nuova, "SD", 9);
		 	 $s.=FormatStr(" ", "SD", 30);
		 	 $s.=FormatStr(" ", "SD", 80);
		 	 $s.=FormatStr(substr($extra, 31, 8), "SD", 8);
		 	 $s.=FormatStr($matricola, "SD", 9);
		 	 $s.=FormatStr($fabbricante, "SD", 3);
		 	 $s.=FormatStr($stato_cont, "SD", 3);
		 	 $s.=FormatStr($lettura_forzata, "N", 1);
		 	 $s.=FormatStr(substr($extra, 39, 22), "SD", 22);
		 	 $s.=FormatStr($cifre, "N", 1);
		 	 $s.=FormatStr(substr($extra, 61, 3), "SD", 3);
		 	 $s.=FormatStr($rs["utente"], "SD", 60);
		 	 $s.=FormatStr($rs["indirizzo"], "SD", 60);
		 	 $s.=FormatStr($rs["localita"], "SD", 25);
		 	 $s.=FormatStr($rs["cap"], "SD", 5);
		 	 $s.=FormatStr($rs["provincia"], "SD", 2);
		 	 $s.=FormatStr(trim(substr($extra, 64, 7)), "SS", 7);
		 	 $s.=FormatStr(trim(substr($extra, 71, 7)), "SS", 7);
		 	 $s.=FormatStr($rs["terminale"], "SD", 12);
		 	 $s.=FormatStr(substr($extra, 78, 7), "SD", 7);
		 	 $s.=FormatStr($flag_note_lett, "SD", 1);
		 	 $s.=FormatStr($data_primo_passaggio, "SD", 8);
		 	 $s.=FormatStr($ora_primo_passaggio, "SD", 4);
		 	 $s.=FormatStr($data_secondo_passaggio, "SD", 8);
		 	 $s.=FormatStr($ora_secondo_passaggio, "SD", 4);
		 	 $s.=FormatStr($tentativi, "N", 1);
		 	 $s.=FormatStr($latitudine, "SD", 9);
		 	 $s.=FormatStr($longitudine, "SD", 9);
		 	 $s.=FormatStr($flag_georef, "SS", 1);
		 	 $s.=FormatStr($flag_no_georef, "SS", 1);
		 	 $s.=FormatStr($cifre, "N", 1);
		 	 $s.=FormatStr($flag_cifre, "N", 1);
		 	 $s.=FormatStr(substr($extra, 85, 70), "SD", 70);
		 	 $s.=FormatStr(substr($extra, 155, 1), "SD", 1);
		 	 $s.=FormatStr($matricola_nuova_vuota, "SD", 9);
		 	 $s.=FormatStr($flag_mat_nuova, "N", 1);
		 	 $s.=FormatStr($flag_cartolina, "SD", 1);
		 	 
		 	 } else {
		 	 
		 	 $s=FormatStr($rs["codice_utente"], "N", 10);
		 	 $s.=FormatStr($pdr, "N", 10);
		 	 $s.=FormatStr(substr($extra, 0, 14), "SD", 14);
		 	 $s.=FormatStr($data_lettura, "SD", 8);
		 	 $s.=FormatStr($rs["lettura_precedente_ente"], "N", 7);
		 	 $s.=FormatStr($lettura, "N", 7);
		 	 $s.=FormatStr(" ", "SD", 1);
		 	 $s.=FormatStr($segn1, "SD", 2);
		 	 $s.=FormatStr($mancata_lettura, "SD", 2);
		 	 $s.=FormatStr($segn_cambio_cont, "SD", 2);
		 	 $s.=FormatStr($cambio_cont, "SD", 1);
		 	 $s.=FormatStr(substr($extra, 23, 8), "SD", 8);
		 	 $s.=FormatStr($lettura_cambio_cont, "N", 7);
		 	 $s.=FormatStr($data_cambio_cont, "SD", 8);
		 	 $s.=FormatStr($matricola_nuova, "SD", 9);
		 	 $s.=FormatStr($nota, "SD", 30);
		 	 $s.=FormatStr($note, "SD", 80);
		 	 $s.=FormatStr(substr($extra, 31, 8), "SD", 8);
		 	 $s.=FormatStr($matricola, "SD", 9);
		 	 $s.=FormatStr($fabbricante, "SD", 3);
		 	 $s.=FormatStr($stato_cont, "SD", 3);
		 	 $s.=FormatStr($lettura_forzata, "N", 1);
		 	 $s.=FormatStr(substr($extra, 39, 22), "SD", 22);
		 	 $s.=FormatStr($cifre, "N", 1);
		 	 $s.=FormatStr(substr($extra, 61, 3), "SD", 3);
		 	 $s.=FormatStr($rs["utente"], "SD", 60);
		 	 $s.=FormatStr($rs["indirizzo"], "SD", 60);
		 	 $s.=FormatStr($rs["localita"], "SD", 25);
		 	 $s.=FormatStr($rs["cap"], "SD", 5);
		 	 $s.=FormatStr($rs["provincia"], "SD", 2);
		 	 $s.=FormatStr(trim(substr($extra, 64, 7)), "SS", 7);
		 	 $s.=FormatStr(trim(substr($extra, 71, 7)), "SS", 7);
		 	 $s.=FormatStr($rs["terminale"], "SD", 12);
		 	 $s.=FormatStr(substr($extra, 78, 7), "SD", 7);
		 	 $s.=FormatStr($flag_note_lett, "SD", 1);
		 	 $s.=FormatStr($data_primo_passaggio, "SD", 8);
		 	 $s.=FormatStr($ora_primo_passaggio, "SD", 4);
		 	 $s.=FormatStr($data_secondo_passaggio, "SD", 8);
		 	 $s.=FormatStr($ora_secondo_passaggio, "SD", 4);
		 	 $s.=FormatStr($tentativi, "N", 1);
		 	 $s.=FormatStr($latitudine, "SD", 9);
		 	 $s.=FormatStr($longitudine, "SD", 9);
		 	 $s.=FormatStr($flag_georef, "SS", 1);
		 	 $s.=FormatStr($flag_no_georef, "SS", 1);
		 	 $s.=FormatStr($cifre, "N", 1);
		 	 $s.=FormatStr($flag_cifre, "N", 1);
		 	 $s.=FormatStr(substr($extra, 85, 70), "SD", 70);
		 	 $s.=FormatStr(substr($extra, 155, 1), "SD", 1);
		 	 $s.=FormatStr($matricola_nuova_vuota, "SD", 9);
		 	 $s.=FormatStr($flag_mat_nuova, "N", 1);
		 	 $s.=FormatStr($flag_cartolina, "SD", 1);
			 }	
		 	 break;

	 }	
 	 //die("Fabbricante=".$fabbricante."<br>S: ".$s);
 	 //die ("<pre>[$flag_mat_nuova][$flag_cartolina]</pre>");
 	 if (strlen($s)!=571){die('<pre><font face="courier new" size=1><b>Errore lunghezza record lavorazione=[$progressivo] pdr=['.$pdr.']<br><br>'.$s.'<br><br>Extra:'.$extra.'</pre>');}
 	 $s.=chr(13).chr(10);
 	 
 	 fwrite($FILE_LETTURE, $s);
 	 
 } // While spedizione Letture
 
 fclose($FILE_LETTURE);
 
 //rename ($dirZip.$nomeFile, $dirZip."UNIX_".$nomeFile);
 //unix2dos $dirZip."UNIX_".$nomeFile $dirZip.$nomeFile;
 
 // Ora zippo il file txt nella stessa cartella
 $archive = new PclZip($dirZip.$nomeZip);
 //$archive->create($dirZip.$nomeFile);
 $archive->add($dirZip.$nomeFile);
 unlink($dirZip.$nomeFile);



}

function creaZipFoto(){
global $file_id;
global $dirFoto;

  $query="select * from caricofile where caricofile_id=$file_id";	
  $ris=mysql_query($query) or die("Errore selezione file da spedire file_id=[$file_id]: " . mysql_error());
  $rs=mysql_fetch_array($ris);
  $progressivo=$rs["caricofile_progressivo"];
  $buff=explode(".",$rs["caricofile_nome"]);
  $nomeFile=$buff[0];
  
  Apri_DB("system");
  $query="select * from lavorazione where lavorazione_progressivo=$progressivo";
  $ris=mysql_query($query) or die("Errore apertura record lavorazione [$progressivo]: " . mysql_error());
  $rs=mysql_fetch_array($ris);
  $nome_lavorazione=$rs["lavorazione_nome"];
  $ripassi=0;
  $cartoline=0;
  if (substr_count(strtoupper($nome_lavorazione), "RIPASSI")>0){$ripassi=1;}
  if (substr_count(strtoupper($nome_lavorazione), "CARTOLINE")>0){$cartoline=1;} 
  
  $dirZip="zip/foto/";
  $d = dir($dirZip);
  while (false !== ($nomeZip = $d->read())) {if ($nomeZip!="." && $nomeZip!=".."){unlink($dirZip.$nomeZip);}}
  
  $dirZipFoto=$dirZip.date("YmdHns")."_foto";
	if (!file_exists($dirZipFoto)) {mkdir($dirZipFoto, 0777);}
	$dirZipFoto.="/";
  
  Apri_DB("sede");
	
  $nomeZip=date("Ymd")."_".date("His")."_".$nomeFile."_FOTO.zip";
  
  //echo $dirZip.$nomeZip;
  
  
  $query="select * from letture where progressivo=$progressivo and indice_file=$file_id and stato='SPE'";
  $ris=mysql_query($query) or die("Errore apertura letture della lavorazione $progressivo per l'indice file [$file_id]: " . mysql_error());
  while ($rs=mysql_fetch_array($ris)){
  	
  	$sequenza=$rs["sequenza"];
  	$codice_utente=FormatStr($rs["codice_utente"], "N", 10);
  	$periodo_fatturazione=substr($rs["extra"], 0, 6);
  	
  	if (substr($nomeFile, 7, 1)=="A"){
  		$periodo_fatturazione=substr($rs["extra"], 16, 5);
  		$periodo_fatturazione=substr($periodo_fatturazione, -1).substr($periodo_fatturazione, 0, 4);
  	} else {
    	$periodo_fatturazione=substr($periodo_fatturazione, 5, 1).substr($periodo_fatturazione, 0, 4);
    }	
  	
  	$flag_foto_1=$rs["flag_foto_1"];
  	$flag_foto_2=$rs["flag_foto_2"];
  	$flag_foto_3=$rs["flag_foto_3"];
  	
  	GetPercorsoFotoFTP($dirFoto, $progressivo, $sequenza, $Percorso_Singola_Foto);
  	
  	//echo $Percorso_Singola_Foto." - ".$dirZip." - ".$progressivo." - ".$sequenza."<br>";
  	
  	if ($flag_foto_1==1){AddFotoToZip($Percorso_Singola_Foto, $dirZipFoto, $progressivo, $sequenza, "01", $periodo_fatturazione, $ripassi, $cartoline, $codice_utente, $archive); }
  	if ($flag_foto_2==1){AddFotoToZip($Percorso_Singola_Foto, $dirZipFoto, $progressivo, $sequenza, "02", $periodo_fatturazione, $ripassi, $cartoline, $codice_utente, $archive); }
  	if ($flag_foto_3==1){AddFotoToZip($Percorso_Singola_Foto, $dirZipFoto, $progressivo, $sequenza, "03", $periodo_fatturazione, $ripassi, $cartoline, $codice_utente, $archive); }
  	
  }
  
 
  $archive = new PclZip($dirZip.$nomeZip);
  $archive->create($dirZipFoto);
  
  $dir = dir($dirZipFoto);
	while (($file = $dir->read()) !== false){if ($file!="." && $file!=".."){unlink($dirZipFoto.$file);}}
	rmdir($dirZipFoto);

}


function creaZipFotoAceaPinerolese(){
global $file_id;

  $query="select * from caricofile where caricofile_id=$file_id";	
  $ris=mysql_query($query) or die("Errore selezione file da spedire file_id=[$file_id]: " . mysql_error());
  $rs=mysql_fetch_array($ris);
  $progressivo=$rs["caricofile_progressivo"];
  $buff=explode(".",$rs["caricofile_nome"]);
  $nomeFile=$buff[0];
  
  Apri_DB("system");
  $query="select * from lavorazione where lavorazione_progressivo=$progressivo";
  $ris=mysql_query($query) or die("Errore apertura record lavorazione [$progressivo]: " . mysql_error());
  $rs=mysql_fetch_array($ris);
  $nome_lavorazione=$rs["lavorazione_nome"];
  $ripassi=0;
  $cartoline=0;
  if (substr_count(strtoupper($nome_lavorazione), "RIPASSI")>0){$ripassi=1;}
  if (substr_count(strtoupper($nome_lavorazione), "CARTOLINE")>0){$cartoline=1;} 
  
  
  $dirZip="zip_aceapinerolese/foto/";
  $d = dir($dirZip);
  while (false !== ($nomeZip = $d->read())) {if ($nomeZip!="." && $nomeZip!=".."){unlink($dirZip.$nomeZip);}}
  
  $dirZipFoto=$dirZip.date("YmdHns")."_foto";
	if (!file_exists($dirZipFoto)) {mkdir($dirZipFoto, 0777);}
	$dirZipFoto.="/";
  
  Apri_DB("sede");
	$dirFoto="../GEA_4/foto/ego_meters_torino/";
  $nomeZip=date("Ymd")."_".date("His")."_".$nomeFile."_FOTO.zip";
  
  //echo $dirZip.$nomeZip;
  
  
  $query="select * from letture where progressivo=$progressivo and indice_file=$file_id and stato='SPE'";
  $ris=mysql_query($query) or die("Errore apertura letture della lavorazione $progressivo per l'indice file [$file_id]: " . mysql_error());
  while ($rs=mysql_fetch_array($ris)){
  	
  	$sequenza=$rs["sequenza"];
  	$codice_utente=FormatStr($rs["codice_utente"], "N", 10);
  	$periodo_fatturazione=substr($rs["extra"], 0, 6);
  	$periodo_fatturazione=substr($periodo_fatturazione, 5, 1).substr($periodo_fatturazione, 0, 4);
  	//die("extra=".$rs["extra"]." periodo_fatturazione=$periodo_fatturazione");
  	
  	$flag_foto_1=$rs["flag_foto_1"];
  	$flag_foto_2=$rs["flag_foto_2"];
  	$flag_foto_3=$rs["flag_foto_3"];
  	
  	GetPercorsoFotoFTP($dirFoto, $progressivo, $sequenza, $Percorso_Singola_Foto);
  	
  	//echo $Percorso_Singola_Foto." - ".$dirZip." - ".$progressivo." - ".$sequenza."<br>";
  	
  	if ($flag_foto_1==1){AddFotoToZipAceaPinerolese($Percorso_Singola_Foto, $dirZipFoto, $progressivo, $sequenza, "01", $periodo_fatturazione, $ripassi, $cartoline, $codice_utente, $archive); }
  	if ($flag_foto_2==1){AddFotoToZipAceaPinerolese($Percorso_Singola_Foto, $dirZipFoto, $progressivo, $sequenza, "02", $periodo_fatturazione, $ripassi, $cartoline, $codice_utente, $archive); }
  	if ($flag_foto_3==1){AddFotoToZipAceaPinerolese($Percorso_Singola_Foto, $dirZipFoto, $progressivo, $sequenza, "03", $periodo_fatturazione, $ripassi, $cartoline, $codice_utente, $archive); }
  	
  }
  
 
  $archive = new PclZip($dirZip.$nomeZip);
  $archive->create($dirZipFoto);
  
  $dir = dir($dirZipFoto);
	while (($file = $dir->read()) !== false){if ($file!="." && $file!=".."){unlink($dirZipFoto.$file);}}
	rmdir($dirZipFoto);

}


function AddFotoToZipAceaPinerolese($Percorso_Singola_Foto, $dirZip, $progressivo, $sequenza, $id_foto, $periodo_fatturazione, $ripassi, $cartoline, $codice_utente, &$archive){
	$progressivo=FormatStr($progressivo, "N", 6);
	$sequenza=FormatStr($sequenza, "N", 6);
	$pattern=$progressivo."_".$sequenza."_".substr($id_foto, -1)."_";
	$progfoto=$id_foto;
	if ($ripassi==1){$progfoto="R".substr($progfoto, -1);}
	if ($cartoline==1){$progfoto="C".substr($progfoto, -1);}
	$nomeFinaleFoto=FormatStr($codice_utente, "N", 10).FormatStr($periodo_fatturazione, "N", 5).$progfoto.".JPG";
	$dir = dir($Percorso_Singola_Foto);
	$buffFile="";
	while (($file = $dir->read()) !== false){
	 if ($file!="." && $file!=".."){
		if (substr($file, 0, strlen($pattern))==$pattern){
	    if ($buffFile!=""){
	     if ($buffFile<$file){$buffFile=$file;} 
	    } else {$buffFile=$file;}
    }
	 }
	}
	
	$file=$buffFile;
	 
	if (strlen($file)>0){ 
	 if (!copy($Percorso_Singola_Foto.$file, $dirZip.$nomeFinaleFoto)) {
	  die("Errore copia foto [$Percorso_Singola_Foto$file] to [$dirZip$nomeFinaleFoto]");
	 } 
	} 
	
}



function AddFotoToZip($Percorso_Singola_Foto, $dirZip, $progressivo, $sequenza, $id_foto, $periodo_fatturazione, $ripassi, $cartoline, $codice_utente, &$archive){
	$progressivo=FormatStr($progressivo, "N", 6);
	$sequenza=FormatStr($sequenza, "N", 6);
	$pattern=$progressivo."_".$sequenza."_".substr($id_foto, -1)."_";
	$progfoto=$id_foto;
	if ($ripassi==1){$progfoto="R".substr($progfoto, -1);}
	if ($cartoline==1){$progfoto="C".substr($progfoto, -1);}
	$nomeFinaleFoto=FormatStr($codice_utente, "N", 10).FormatStr($periodo_fatturazione, "N", 5).$progfoto.".JPG";
	$dir = dir($Percorso_Singola_Foto);
	$buffFile="";
	while (($file = $dir->read()) !== false){
	 if ($file!="." && $file!=".."){
		if (substr($file, 0, strlen($pattern))==$pattern){
	    if ($buffFile!=""){
	     if ($buffFile<$file){$buffFile=$file;} 
	    } else {$buffFile=$file;}
    }
	 }
	}
	
	$file=$buffFile;
	 
	if (strlen($file)>0){ 
	 if (!copy($Percorso_Singola_Foto.$file, $dirZip.$nomeFinaleFoto)) {
	  die("Errore copia foto [$Percorso_Singola_Foto$file] to [$dirZip$nomeFinaleFoto]");
	 } 
	} 
}

function GetPercorsoFotoFTP($InitPath, $progressivo, $sequenza, &$percorso){
	$percorso=$InitPath;
	if(!is_dir($percorso)) {mkdir($percorso, 0777);}
	$percorso.="lav_".formatStr($progressivo, "N", 6)."/";
	if(!is_dir($percorso)) {mkdir($percorso, 0777);}
	$progressivo=formatStr($progressivo, "N", 6);
	$sequenza=formatStr($sequenza, "N", 6);
	//$nomeFile=$progressivo."_".$sequenza."_".$id_foto."_";
	for ($i=1; $i<=4; $i++){
		$cartella=substr($sequenza, $i-1, 1).substr("000000", 0, 6-$i);
		$percorso.=$cartella."/";
		if(!is_dir($percorso)) {mkdir($percorso , 0777);}
	}
}

function CLEAN_VAR($buff){
	$buff=str_replace(",", "", $buff);
	$buff=str_replace(".", "", $buff);
	$buff=str_replace("(", "", $buff);
	$buff=str_replace(")", "", $buff);
	$buff=str_replace("'", "", $buff);
	$buff=str_replace(chr(34), "", $buff);
	$buff=str_replace("+", "", $buff);
	$buff=str_replace("/", "", $buff);
	$buff=str_replace("*", "", $buff);
	return $buff;
}

function GetLocalitaSmat($impianto, $giro, $default){
	$localita=$default; 
	
	
	switch ($impianto){
												   	  
		case "001": $localita="TORINO"; break;
		case "008": $localita="Collegno"; break;
		case "011": $localita="Grugliasco"; break;
		case "029": $localita="Rivoli"; break;
		case "030": $localita="Buttigliera"; break;
		case "031": $localita="Rosta"; break;
		case "037": $localita="Beinasco"; break;
		case "038": $localita="Bruino"; break;
		case "039": $localita="Candiolo"; break;
		case "040": $localita="Giaveno"; break;
		case "044": $localita="Piossasco"; break;
		case "045": $localita="Rivalta"; break;
		case "046": $localita="Sangano"; break;
		case "049": $localita="Avigliana"; break;
		case "052": $localita="Villarbasse"; break;
		case "055": $localita="Castagnole"; break;
		case "056": $localita="Coazze"; break;
		case "057": $localita="Trana"; break;
		case "059": $localita="Condove"; break;
		case "064": $localita="Reano"; break;
		case "065": $localita="Valgioie"; break;
		case "070": $localita="Sant'Ambrogio"; break;
		case "072": $localita="Villardora"; break;
		case "076": $localita="Bussoleno"; break;
		case "077": $localita="Caprie"; break;
		case "082": $localita="Susa"; break;
		case "091": $localita="Chiusa S. Michele"; break;
		case "096": $localita="Vaie"; break;
		case "098": $localita="Borgone Susa"; break;
		case "113": $localita="Leini"; break;
		case "116": $localita="Rubiana"; break;
		case "120": $localita="Almese"; break;
		case "124": $localita="Riva presso Ghieri"; break;
		case "125": $localita="San Didero"; break;
		case "204": $localita="Meana di Susa"; break;
		case "222": $localita="Sant'Antonino"; break;
		case "264": $localita="None"; break;
		case "285": $localita="San Secondo"; break;
		case "289": $localita="Torre Pellice"; break;
		case "292": $localita="Vigone"; break;
		case "294": $localita="Villar Pellice"; break;
		case "296": $localita="Virle"; break;
		case "297": $localita="Mattie"; break;
		case "298": $localita="San Giorio di Susa"; break;
		
	}	
	
	if ($impianto=="001"){
		switch ($giro){
			case "480":
			case "481":
			case "482":
			case "485":
			case "490":
			case "492":
			case "495":
			case "500":
			case "502":
			case "504":
			case "506":
			case "508":
			case "510":
			case "512":
			case "514":
			case "516":
			case "518":
			case "520":
			case "525":
			case "527":
			case "528":
			case "530":
			case "531":
			case "533":
			case "534":
			case "535":
			case "537":
			case "538":
			case "540":
			case "542":
			case "543":
			case "547":
			case "548": $localita="TORINO GRUPPO 1"; break;
			case "550":
			case "551":
			case "553":
			case "555":
			case "557":
			case "560":
			case "562":
			case "565":
			case "567":
			case "569":
			case "570":
			case "571":
			case "573":
			case "575":
			case "576":
			case "579":
			case "580":
			case "582":
			case "583":
			case "586":
			case "587":
			case "588":
			case "591":
			case "595":
			case "597": $localita="TORINO GRUPPO 1A"; break;
			case "600":
			case "601":
			case "603":
			case "604":
			case "606":
			case "607":
			case "610":
			case "611":
			case "613":
			case "614":
			case "615":
			case "617":
			case "618":
			case "619":
			case "621":
			case "623":
			case "625":
			case "630":
			case "631":
			case "634":
			case "635":
			case "638":
			case "640":
			case "644":
			case "645": $localita="TORINO GRUPPO 2"; break;
			case "650":
			case "652":
			case "653":
			case "654":
			case "656":
			case "658":
			case "659":
			case "660":
			case "661":
			case "662":
			case "665":
			case "667":
			case "668":
			case "670":
			case "671":
			case "672":
			case "673":
			case "675":
			case "677":
			case "678":
			case "680":
			case "682":
			case "683":
			case "690":
			case "691":
			case "692": $localita="TORINO GRUPPO 2A"; break;
			case "700":
			case "702":
			case "705":
			case "707":
			case "710":
			case "712":
			case "714":
			case "715":
			case "720":
			case "730":
			case "750":
			case "751":
			case "753":
			case "755":
			case "757":
			case "760":
			case "763":
			case "765":
			case "767":
			case "770":
			case "772":
			case "775":
			case "780":
			case "782":
			case "785":
			case "788": $localita="TORINO GRUPPO 3"; break;
			case "800":
			case "802":
			case "805":
			case "806":
			case "807":
			case "808":
			case "810":
			case "811":
			case "812":
			case "815":
			case "817":
			case "818":
			case "819":
			case "820":
			case "822":
			case "823":
			case "825":
			case "826":
			case "828":
			case "830":
			case "831":
			case "835":
			case "836":
			case "840":
			case "845": $localita="TORINO RUPPO 4"; break;
			case "850":
			case "851":
			case "854":
			case "855":
			case "857":
			case "858":
			case "860":
			case "862":
			case "863":
			case "865":
			case "867":
			case "868":
			case "870":
			case "871":
			case "872":
			case "874":
			case "875":
			case "876":
			case "880":
			case "883":
			case "885":
			case "887":
			case "889":
			case "890":
			case "895": $localita="TORINO GRUPPO 4A"; break;
			case "900":
			case "902":
			case "905":
			case "907":
			case "908":
			case "910":
			case "915":
			case "920":
			case "925":
			case "928":
			case "930":
			case "935":
			case "942":
			case "945":
			case "950":
			case "955":
			case "960":
			case "956":
			case "970":
			case "975":
			case "980":
			case "985":
			case "986": $localita="TORINO GRUPPO 5"; break;
			case "991":
			case "992":
			case "993":
			case "994":
			case "995":
			case "996":
			case "997":
			case "998":
			case "999": $localita="TORINO SPECIALI"; break;
		}	
	} // Torino	
	
	if ($impianto=="029"){
		switch ($giro){
			case "001":
			case "002":
			case "003":
			case "004":
			case "006":
			case "018":
			case "019":
			case "021":
			case "022": $localita="RIVOLI 1"; break;
			case "007":
			case "008":
			case "009":
			case "011":
			case "012":
			case "013":
			case "014":
			case "016": $localita="RIVOLI 2"; break;
		}	
	} // Rivoli
	return $localita;
}


?>
