<?php

// Server MySql
$serversql = getenv('DB_HOST');
$utentedb = getenv('DB_USERNAME');
$passworddb = getenv('DB_PASSWORD');

// Database
$database = getenv('DB_DATABASE_FOTOLETTURE');
$database_system = getenv('DB_DATABASE');
$database_sede = getenv('SEDE_DATABASE');;
//$database_sede= isset($_SESSION["ses-ut-sede-db"]) ? $database_sede= $_SESSION["ses_ut_sede_db"] : $database_sede="ecoservim_smat_gea_4_torino"; 

$nome_cliente=getenv('APP_CUSTOMER');
$radice_dir_foto = "foto/";
$radice_dir_zip = "zip/";
$radice_dir_report = "report/";
$radice_dir_restituzioni = 'restituzioni_smat/';

$margin_foto=1;
$larghezza_dati=(($lato_foto*3)-9);
$larghezza_label=160;
$altezza_dato=333;
$altezza_mappa=$lato_foto+$altezza_dato+8;

	
function Apri_DB($cosa){
	global $serversql;
	global $utentedb;
	global $passworddb;
	global $database;
	global $database_system;
	global $database_sede;
	global $connessione;

  	switch ($cosa){
		case "system": $db = $database_system; break;
		case "sede": $db = $database_sede; break;
		case "fotoletture": $db = $database; break;
  	}

  	$connessione=mysql_pconnect($serversql,$utentedb,$passworddb) or exit ("Impossibile connettersi al database, verificare i dati di accesso");
  	mysql_select_db($db,$connessione) or exit ("Nome del database ".$cosa." non trovato !".$db);
}  

?>
