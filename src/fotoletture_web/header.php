<?php

echo'
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
	<html>
	<head>
	<title>Fotoletture '.$nome_cliente.'</title>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
	<meta name="generator" content="HAPedit 3.0">
	
	<meta http-equiv="cache-control" content="max-age=0" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
	<meta http-equiv="pragma" content="no-cache" />

      
	<link href="fogliostile_fotoletture.css" rel="StyleSheet" type="text/css">
	
	   	 
	
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&language=it&region=IT&key=AIzaSyDKm51Gi15Xo0imBX-cQvdbUa31MyPdi0o"></script>
  	<script type="text/javascript">
  	
  	var letturisti = '.json_encode($_SESSION['ses_letturisti']).';
  	
    var browserSupportFlag =  new Boolean();

    var iconGreen = new google.maps.MarkerImage(\'images/images/mm_20_green.png\',
      new google.maps.Size(12, 20),
      new google.maps.Point(0,0),
      new google.maps.Point(6, 20));

    var iconRed = new google.maps.MarkerImage(\'images/images/mm_20_red.png\',
      new google.maps.Size(12, 20),
      new google.maps.Point(0,0),
      new google.maps.Point(6, 20)
      );

    var iconYellow= new google.maps.MarkerImage(\'images/images/mm_20_red.png\',
      new google.maps.Size(12, 20),
      new google.maps.Point(0,0),
      new google.maps.Point(6, 20)
      );

    var shadowIcon = new google.maps.MarkerImage(\'images/images/mm_20_shadow.png\',
      // The shadow image is larger in the horizontal dimension
      // while the position and offset are the same as for the main image.
      new google.maps.Size(22, 20),
      new google.maps.Point(0,0),
      new google.maps.Point(6, 20)
      );
    

    var customIcons = [];
    customIcons["ultima"] = iconRed;
    customIcons["buona"] = iconGreen;
    customIcons["intermedia"] = iconYellow;
    customIcons["geo"] = iconYellow;
      
    var map;

    function createMarker(point, name, address, type) {
        var marker = new google.maps.Marker({position: point, map: map, icon: customIcons[type]  ,shadow: shadowIcon ,title: name }); 
        var html = "<b>" + name + "</b> <br/>" + address;
        var miaInfo = new google.maps.InfoWindow({ content: html });
        google.maps.event.addListener(marker, \'click\', function() {miaInfo.open(map,marker); });
        return true;
      }
    
    
   	function initialize(latitudine, longitudine) {
    	
       
  	}
  	
 
     
  	function Mappa_Letturisti_Sul_Campo(lista_coordinate, latcentro, lngcentro) {
    	  
    	  centroMappa= new google.maps.LatLng(latcentro,lngcentro);
    	  var myOptions = {
            zoom: 15,
            center: centroMappa,
            mapTypeId: google.maps.MapTypeId.ROADMAP        
            };
        map = new google.maps.Map(document.getElementById("mappadigoogle"), myOptions);
      
    	  
    	  var term;
		    var latitudine;
		    var longitudine;
		    var data_lettura;
		    var ora_lettura;
    	  
        var array_coordinate = lista_coordinate.split("@"); 
        var array_Length = array_coordinate.length;
				for (var i = 0; i < array_Length; i++) {
				    buff = array_coordinate[i].split("|");
				    "$term|$latitudine|$longitudine|$data_lettura|$ora_lettura@";
				    term=buff[0];
				    latitudine=buff[1];
				    longitudine=buff[2];
				    data_lettura=buff[3];
				    ora_lettura=buff[4];
				    indirizzo=buff[5];
				    
				    Disegna_Punto_Su_Mappa(map , term, latitudine, longitudine, data_lettura, ora_lettura, indirizzo);
				}
    	  
        
  	 
       
  	}
  	
  	function Disegna_Punto_Su_Mappa(map, term, latitudine, longitudine, data_lettura, ora_lettura, indirizzo){
  	 
  	 var point = new google.maps.LatLng(parseFloat(latitudine),parseFloat(longitudine));
		 //createMarker(point, \'ciccio\', \'\', \'buona\');
		 var myLatLng = new google.maps.LatLng(parseFloat(latitudine), parseFloat(longitudine))
		 var Marker_Worker = new google.maps.Marker({ 
		 	                          position: myLatLng,  
		 	                          map: map, 
		 	                          icon: \'images/marker_worker.png\' }); 
		 	                          
     var info="terminale= <b>"+term+"</b>"+"<br>Data ultima lettura effettuata= <b>"+data_lettura+"</b><br>"+"Ora ultima lettura effettuata= <b>"+ora_lettura+"</b><br>Ultimo indirizzo visitato= <b>"+indirizzo+"</b>";
     var info = new google.maps.InfoWindow({
       content:info.bold()
     });
     info.open(map, Marker_Worker);
     
     //google.maps.event.addListener(Marker_Worker, "click", function (e) { info.open(map, this); });      	
        //google.maps.event.addListener(map, \'click\', 
        //  function( event){
        //    alert("OK memorizzata coordinata \n lat="+event.latLng.lat()+" \n long="+event.latLng.lng()+" \n\n Ora puoi usare funzione \'Pesca Coordinata dalla mappa\' se presente");
        //    document.getElementById(\'latitudine\').innerHTML=event.latLng.lat();
        //    document.getElementById(\'longitudine\').innerHTML=event.latLng.lng();
        //  }
        //);  
  	}
  
  
  	</script>
	

	  

  <!-- Easy UI -->  
	  <link rel="stylesheet" type="text/css" href="EasyUi/themes/default/easyui.css">
		<link rel="stylesheet" type="text/css" href="EasyUi/themes/icon.css">
		<script type="text/javascript" src="EasyUi/jquery.min.js"></script>
		<script type="text/javascript" src="EasyUi/jquery-1.8.3.min.js"></script>
		
		<script type="text/javascript" src="EasyUi/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="EasyUi/locale/easyui-lang-it.js"></script>
		<script src="scripts/jquery.elevatezoom.js"></script>
		
      
		
		<script type="text/javascript">
		   
		  var mem_foto_1=""; 
		  var mem_foto_2="";
		  var mem_foto_3="";
		
		  var ShowCheckedFiles=0;
		  function ShowHideCheckedFiles(){
		  	if (ShowCheckedFiles==1){
		  		document.getElementById("icona_checkedfile")
		  		ShowCheckedFiles=0
		  		NomeFile="bt_eye_single.png";
		    } else {
		    	ShowCheckedFiles=1
		    	NomeFile="bt_eye_all.png";
		  	}
		  	
		  	var image_x = document.getElementById("icona_checkedfile");
		    image_x.parentNode.removeChild(image_x);
     	  document.getElementById("div_icona_checkedfile").innerHTML = \'<img src="images/\'+NomeFile+\'"  id="icona_checkedfile" onclick="ShowHideCheckedFiles()" />\';
     	  
     	  doSearch();
		  	
		  }
		
			function doSearch(){
				$("#dg").datagrid("load",{
					comune_smat: $("#comune_smat").val(),
					rTipoLetture: $("input:checked").val(),
					caricofile_nome: $("#caricofile_nome").val(),
					cFileChecked: ShowCheckedFiles,
					periodo: $("#periodo").val()
				});
			}
			
			function doReset(){
				$("#comune_smat").val("");
				$("#rTipoLetture1").prop("checked", true);
				$("#rTipoLetture2").attr("checked", false);
				$("#rTipoLetture3").attr("checked", false);
				$("#rTipoLetture4").attr("checked", false);
				$("#caricofile_nome").val("");
				$("#periodo").val("");
				doSearch();
			}
			
			function Zip_Link(val, row, indice){
		  }
		
		  function CeccaFile(id, valore){
	  		var xhr = new XMLHttpRequest();
		    xhr.onreadystatechange = function(){ if(xhr.readyState == 4 && xhr.status == 200) { doSearch(); } }
			  url="check_file.php?caricofile_id="+id+"&valore="+valore;
			   xhr.open("GET", url, true);
			   xhr.send();
			}

      $(function(){
          		 
      	  var dg = $("#dg").datagrid();
					dg.datagrid({';
            
					  switch($_SESSION["ses-ut-ente"]){
					  	case 1: echo'url:"gestione_getdata_smat.php",'; break;
					  	case 17: echo'url:"gestione_getdata_smat.php",'; break;
					  	default: echo'url:"gestione_getdata_smat.php",'; break;
					  }
					  
					  echo'	
						title:"File Smat",
						iconCls:"icon-search",
						toolbar:"#tb",
						width:"100%",
						height:"311",
						singleSelect:true,
						remoteSort:false,
						rownumbers:false,
						pagination:true,
						collapsible:true,
	          fitColumns: true,
						columns:[[
						  {field:"eye",title:"",width:30,align:"center",editor:{type:\'numberbox\'}, 
						  	formatter:function(val, row, idx){
						     	if (row.caricofile_checked==1){
						     		var e = "<a href=\'javascript:CeccaFile("+row.caricofile_id+",0);\'><img src=\'images/eye_off.png\'></a>"; 
						     	} else {
						     		var e = "<a href=\'javascript:CeccaFile("+row.caricofile_id+",1);\'><img src=\'images/eye_on.png\'></a>"; 
						     	}
						     	return e;
						     }
						  },';
						  
						  // Se l'utilizzatore � abilitato gli permetto il download file
						  if ($_SESSION["ses-download_file"]==1){
						     	echo'
								  {field:"zip",title:"ZIP",width:30,align:"center", editor:{type:\'numberbox\'}, 
								     formatter:function(val, row, idx){
									     	if (row.tot_letture==row.spe){
									     		//if (substr(strtoupper(row.caricofile_nome), -4)==".INP") {
									     		//	var e = "<a href=\'zip.php?fase=dwnld_all&file_id="+row.caricofile_id+"\' target=\'_blank\'><img src=\'images/zip.png\'></a>"; 
									     		//     }
									     		//else {	
									     		//	var e = "<a href=\'zip.php?fase=dwnld_all&file_id="+row.caricofile_id+"\' target=\'_blank\'><img src=\'images/zip.png\'></a>"; 
									     		var e = "<a href=\'zip_smat2017.php?fase=dwnld_all&file_id="+row.caricofile_id+"\' target=\'_blank\'><img src=\'images/zip.png\'></a>"; 
									     		//     }
									     	} else {
									     		var e = "<img src=\'images/zipoff.png\'>"; 
									     	}
								     	return e;
								     }
								  },';
							}
							echo'
							{field:"caricofile_id",title:"ID",align:"center",width:60,sortable:true},
							{field:"caricofile_nome",title:"FILE",width:300,align:"left",sortable:true},';
							
							if ($_SESSION["ses-ut-ente"]==17){
								echo'
									{field:"caricofile_localita",title:"COMUNE",width:300,align:"left",sortable:true},
									{field:"impianto",title:"IMPIANTO",width:60,align:"center",sortable:true},
									{field:"giro",title:"GIRO",width:60,align:"center",sortable:true},
									{field:"periodo",title:"PERIODO",width:60,align:"left",sortable:true},
								';
							}
							
							echo'		
							{field:"caricofile_data_in",title:"DATA IN",width:80,align:"left",sortable:true},
							{field:"caricofile_data_out",title:"DATA OUT",width:80,align:"left",sortable:true},
							{field:"tot_letture",title:"TOT LETT",width:60,align:"left",sortable:true},
							{field:"no_spe",title:"IN LETT",width:60,align:"left",sortable:true},
							{field:"spe",title:"RICONS.",width:60,align:"left",sortable:true},
							{field:"perc",title:"PERC",width:60,align:"left", editor:{type:\'numberbox\'}, 
						     formatter:function(val, row, idx){
  				     		var e=Math.round(((parseInt(row.no_spe)+parseInt(row.spe))/parseInt(row.tot_letture))*100)+" %";
						     	return e;
						     }
						  },';
						  
						  if ($_SESSION["ses-ut-ente"]==17){
						  	echo'{field:"terminali",title:"LETTURISTI",width:60,align:"left",sortable:true}';
						  }								
							echo'
						]]
					});
			});
			
			function doSearchLetture(){
				var row = $("#dg").datagrid("getSelected");
				var buff="";
		    if (row){buff=row.caricofile_id;}
 			 $("#gl").datagrid("load",{
					pdr: $("#pdr").val(),
					codice_utente: $("#codice_utente").val(),
					matricola: $("#matricola").val(),
					utente: $("#utente").val(),
					segnalazione: $("#segnalazione").val(),
					acc: $("#acc").val(),
					flag: $("#flag").val(),
					caricofile_id: buff
				});
			}
			
			function doResetLetture(){
				$("#pdr").val("");
				$("#codice_utente").val("");
				$("#matricola").val("");
				$("#utente").val("");
				$("#segnalazione").val("");
				$("#acc").val("");
				$("#flag").val("");
				$("#gl").datagrid("load",{
					pdr: $("#pdr").val(),
					codice_utente: $("#codice_utente").val(),
					matricola: $("#matricola").val(),
					utente: $("#utente").val(),
					segnalazione: $("#segnalazione").val(),
					acc: $("#acc").val(),
					flag: $("#flag").val()
				});
			}
			
			$(function(){
      	  var gl = $("#gl").datagrid();
      	   
					gl.datagrid({
						url:"letture_getdata.php",
						title:"Letture",
						iconCls:"icon-edit",
						toolbar:"#tbl",
						width:"100%",
						height:"348",
						singleSelect:true,
						remoteSort:false,
						rownumbers:false,
						pagination:true,
						collapsible:true,
	          fitColumns: true,
						columns:[[
							{field:"matricola_contatore_padre",title:"PDP",align:"left",width:80,sortable:true},
							{field:"codice_utente",title:"COD UT",width:80,align:"left",sortable:true},
							{field:"utente",title:"UTENTE",width:180,align:"left",sortable:true},
							{field:"indirizzo",title:"INDIRIZZO",width:180,align:"left",sortable:true},
							{field:"civico",title:"CIV",width:30,align:"left",sortable:true},
							{field:"est",title:"/",width:30,align:"left", editor:{type:\'numberbox\'}, 
						     formatter:function(val, row, idx){
  				     		var e;
  				     		if(row.barrato){e =row.barrato;}
  				     		if(row.scala){e+=" " +row.scala;}
  				     		if(row.piano){e+=" " +row.piano;}
  				     		if(row.interno){e+=" " +row.interno;}
						     	return e;
						     }
						  },
							{field:"localita",title:"LOCALITA\'",width:80,align:"left",sortable:true},
							{field:"matricola",title:"MATRICOLA",width:80,align:"left",sortable:true},
							{field:"lettura_precedente_ente",title:"LET PRE",width:60,align:"left",sortable:true},
							{field:"letmin",title:"LETMIN",width:60,align:"left",sortable:true},
							{field:"lettura",title:"LETTURA",width:60,align:"left",sortable:true},
							{field:"letmax",title:"LET MAX",width:60,align:"left",sortable:true},
							{field:"data_lettura",title:"DATA",width:60,align:"left",sortable:true},
							{field:"cns",title:"CONSUMO",width:60,align:"left", editor:{type:\'numberbox\'}, 
						     formatter:function(val, row, idx){
  				     		var e="";
  				     		if (isNumeric(row.lettura) && isNumeric(row.lettura_precedente_ente)){e = row.lettura-row.lettura_precedente_ente;}	 
						     	return e;
						     }
						  },
							{field:"acc",title:"ACC",width:30,align:"center",sortable:true},
							{field:"segn1",title:"ESITO",width:40,align:"center",sortable:true},
							{field:"flag",title:"FLAG",width:40,align:"center",sortable:true},
						]],
						onDblClickRow:function(index, row){
							Mostra_Dettagli(row);
						}
					});
					
					
					
					
			    
			});
			
			function FormatStr(s, tipo, lung){
			 stringazza="                                                                                                                                                                ";
			 numerazzo="00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
			  risultato=s;
			 switch (tipo){
			  case "SD": risultato = (s+stringazza).substring(0, lung); break;
			  case "SS": risultato = (stringazza+s).slice((-1)*lung); break;
			  case "N":  risultato = (numerazzo+s).slice((-1)*lung); break;
			 }
			 return risultato;
			}
			
			function Mostra_Dettagli(row){
				
				//document.getElementById("icon-user").style.visibility="visible";
						
	     	progressivo=NUM(row.progressivo);
	     	sequenza=NUM(row.sequenza);
	     	flag_foto_1=NUM(row.flag_foto_1);
	     	flag_foto_2=NUM(row.flag_foto_2);
	     	flag_foto_3=NUM(row.flag_foto_3);
	     	var pdr=TXT(row.matricola_contatore_padre);
	     	var matricola_contatore_padre=TXT(row.matricola_contatore_padre);
	     	var latitudine=TXT(row.latitudine);
	     	var longitudine=TXT(row.longitudine);
	     	var numero_satelliti=TXT(row.numero_satelliti);
	     	var matricola=TXT(row.matricola);
	     	var matricola_nuova=TXT(row.matricola_nuova);
	     	var extra = TXT(row.extra);
	     	var nomefile = TXT(row.caricofile_nome);
	     	var giro = "";
	     	if(nomefile.substr(4, 1)==="A"){
	     					giro=nomefile.substr(5, 3);
	     				}else{
	     				giro=nomefile.substr(4, 3);
	     				}
	     					
        var impianto=nomefile.substr(0, 4);

        var periodo="";  // vecchio tracciato INP extra.substr(4, 2)+"/"+extra.substr(0, 4);
	     	
	     	
	     	
     	
	     	var stringa_campi="letturista|letturista|@"+
	     	                  "PDP|matricola_contatore_padre|@"+
	     	                  "cod ut|codice_utente|@"+
		     	          "utente|utente|@"+
		     	          "indirizzo|indirizzo|@"+
		     	          "civico|civico@"+
		     	          "localit�|localita|@"+
		     	          "imp-giro|ig|@"+
		     	          "periodo|periodo|@"+
		     	          "matricola|matricola|matricola_nuova@"+
		     	          "cifre|cifre|cifre_nuove@"+
		     	          "acc|acc|acc_nuova@"+
		     	          "let pre|let pre|@"+
		     	          "data|data|@"+
		     	          "range|range|@"+
		     	          "LETTURA|lettura|@"+
		     	          "esito|segn1|@"+
		     	          "consumo|consumo|@"+
		     	          "ubicazione Smat|ubicazione|@"+
		     	          "nota Letturista|nota|@"+
		     	          "latitudine|latitudine|@"+
		     	          "longitudine|longitudine|@"+
		     	          "satelliti|numero_satelliti|";
			     	              
	     	var vettore_campi=stringa_campi.split("@");
	     	var singolo_campo;
	     	var lista_etichette="";
	     	var lista_dati="";
	     	var dato="";
	     	coloreRosso="#e29a9a";
	     	for (var i=0; i<vettore_campi.length; i++) {
				    singolo_campo = vettore_campi[i].split("|");
				    lista_etichette+=TXT(singolo_campo[0])+": <br>";
				    
				    switch (singolo_campo[0]){
				    	  case "letturista": dato=row.terminale+" - "+Get_Nome_Letturista(row.terminale); 
				    	                     if (dato=="0 - "){dato="";}
				    	                     break;
				    	    case "localita": GetNomeLocalita(row.localita, row.codice_ente,impianto, giro);                  
				    	    case "imp-giro": dato=impianto+" - "+giro; break;
				    	     case "periodo": dato=periodo; break;
				    	  case "letturista": dato=row.terminale; break;
				    	        case "data": dato=TXT(row.data_lettura)+" - "+TXT(row.ora_lettura); break;
				    	     case "let pre": if (TXT(row.data_lettura_precedente_ente)){dato=TXT(row.data_lettura_precedente_ente)+" - ";}
				    	         						 dato+=TXT(row.lettura_precedente_ente); 
				    	                     break;
				   case "nota Letturista": dato=row.nota.substr(0, 30).trim();  break; 	                     
                 case "note Smat": dato=TXT(row.note);  if (TXT(row.note_nuove).length>0){dato=TXT(row.note_nuove);} dato=dato.substr(0, 80).trim(); break;
                 case "ubicazione Smat": dato=TXT(row.ubicazione);  if (TXT(row.ubicazione_nuova).length>0){dato=TXT(row.ubicazione_nuova);} dato=dato.substr(0, 100).trim(); break;
				    	     case "consumo": dato=Espandi_Consumo(row.flag); break;
				    	       case "range": dato=TXT(row.letmin)+" - "+TXT(row.letmax); break;
				    	      case "civico": if (!(row.civico)){dato="???";}else{dato=row.civico;}
				    	                     if (row.barrato){dato+="/"+row.barrato.replace("/", "");}
				    	                     if (row.scala){dato+=" sc:"+row.scala;}
				    	                     if (row.piano){dato+=" pi:"+row.piano;}
				    	                     if (row.interno){dato+=" in:"+row.interno;}
				    	                     if (row.civico_nuovo){dato="<font color=\'#aa0000\'>"+dato+"</font>"+row.civico_nuovo;}
				    	                     break;
				    	            default: dato=TXT(eval("row."+singolo_campo[1])).substr(0, 30).trim(); break;
				    }
				    
				    
				    if (singolo_campo[2]){if (TXT(eval("row."+singolo_campo[2]))){dato="<font color=\'#aa0000\'>"+TXT(eval("row."+singolo_campo[1]))+"<font color=\'#000000\'> - "+TXT(eval("row."+singolo_campo[2]));}}  
				    // ERRORE Non posso fare substring 30 perch� senn� taglio i tag html
				    lista_dati+=dato+"<br>";
				}
	     	
	     	
	     	document.getElementById("lista_etichette").innerHTML=lista_etichette
	     	document.getElementById("lista_dati").innerHTML=lista_dati
	     	
	      var div = document.getElementById("div-dettagli-dati");
        div.style.backgroundColor="#ccd9e9";
     	  if (TXT(row.flag)=="A"){div.style.backgroundColor =coloreRosso;}
	     	
	     	const photoWidth = '; print($lato_foto); echo '
              const photosContainer = $(\'#foto_main_container\');
              photosContainer.empty();
              const photos = [];

              let index = 1;
              for (const photo of row.foto) {
                  photos.push(\'<div class="foto_container" style="margin: 3px">\' +
                              \'    <div class="div-foto-contatore zoom-image">\' +
                              \'        <img src="\'+photo+\'" data-zoom-image="\'+photo+\'" border="0" height="\'+(photoWidth-50)+\'" width="\'+(photoWidth-50)+\'" class="foto-contatore" style="object-fit: cover"/>\' +
                              \'    </div>\' +
                              \'    <div class="foto_label">Foto \'+index+\'</div>\' +
                              \'    <div class="dwnld_foto"><a href="\'+photo+\'" download="\'+row.pdr+\'_\'+index+\'.jpg" title="ImageName"><img src="images/dwnld_foto_on.png" border="0" alt="[ ]" title="download foto"></a></div>\' +
                              \'</div>\');
                  index++;
              }

              let currentDiv;
              for (let i = 0; i < photos.length; i++) {
                  if (i % 3 === 0) {
                      currentDiv = $(\'<div style="width: \'+(photoWidth*3+18)+\'px;"></div>\');
                      photosContainer.append(currentDiv);
                  }
                  currentDiv.append(photos[i]);
              }
              $(\'.foto-contatore\').each(function(i, obj) {
                  $(obj).elevateZoom({zoomType:"inner",cursor:"crosshair",zoomWindowFadeIn:500,zoomWindowFadeOut:750});
              });

              const rows = Math.ceil(photos.length/3);
              if (rows === 0) {
                  $(\'#div-dettagli-dati\').css({top: \'30px\', height: '; echo $lato_foto + $altezza_dato - 9; echo ' + \'px\'});
                  $(\'.div-mappa-contatore\').css({height: (photoWidth + '; echo $altezza_dato; echo' - 3) + \'px\'});
              } else {
                  $(\'#div-dettagli-dati\').css({top: (photoWidth * rows + (6 * rows) + 31) + \'px\', height: ('; echo $altezza_dato; echo' - (6 * rows) - 9) + \'px\'});
                  $(\'.div-mappa-contatore\').css({height: (photoWidth * rows + '; echo $altezza_dato; echo' - 3) + \'px\'});
              }
	     		     	
	     	Mostra_Mappa(latitudine, longitudine, numero_satelliti);
	    }
	    
	    function GetNomeLocalita(localita,codice_ente,impianto,giro){
	    	if (codice_ente==17){
	    		return localita;
	    	} else {	
	    	  return localita;
	    	}  
	    }
	    
	    function EliminaFotoEZoom(){
	    	$( "div" ).remove( ".zoomContainer" );
	     	var image_x;
	     	image_x = document.getElementById("img_foto_1");
		    image_x.parentNode.removeChild(image_x);
		    image_x = document.getElementById("img_foto_2");
		    image_x.parentNode.removeChild(image_x);
		    image_x = document.getElementById("img_foto_3");
		    image_x.parentNode.removeChild(image_x);
	    }
	    
	    function Espandi_Consumo(flag){
	    	
	    	switch(flag){
	    		case "I": return "Negativo"; break;
	    		case "G": return "Girocontatore"; break;
	    		case "N": return "Nullo"; break;
	    		case "B": return "Basso"; break;
	    		case "L": return "Normale"; break;
	    		case "A": return "Alto"; break;
	    	}
	    }
	    
	    function TXT(buff){
	    	if (buff === undefined || buff === null){
	    		return "";
	    	} else {
	    		return buff;
	    	}
	    }
	    
	    function NUM(buff){
	    	if (buff === undefined || buff === null || isNaN(buff)){
	    		return "0";
	    	} else {
	    		return buff;
	    	}
	    }
	    
	    function Get_Nome_Letturista(term){
	    	nome="";
	    	for (var i=0; i<letturisti.length; i++) {
		    	if (letturisti[i].letturista_numero==term){
		    		nome=letturisti[i].letturista_nome; break; 
		    	}
	    	}
	    	return nome;
	    }
	    
	    function Mostra_Mappa(latitudine, longitudine, numero_satelliti){
	    	latitudine=latitudine.replace(",", ".");
	    	longitudine=longitudine.replace(",", ".");
	    	if (latitudine&&longitudine){
	    		
	    	  centroMappa= new google.maps.LatLng(latitudine,longitudine);
	        var myOptions = {
	          zoom: 17,
	          center: centroMappa,
	          mapTypeControlOptions: {
				        mapTypeControlOptions: {mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.SATELLITE, google.maps.MapTypeId.HYBRID]},
    			      mapTypeControl: true
				     }
	          };
	          
	        map = new google.maps.Map(document.getElementById("mappadigoogle"), myOptions);
	      
			 var point = new google.maps.LatLng(parseFloat(latitudine),parseFloat(longitudine));
			 createMarker(point, \'ciccio\', \'\', \'buona\');
			 var myLatLng = new google.maps.LatLng(parseFloat(latitudine), parseFloat(longitudine))
			 var beachMarker = new google.maps.Marker({ position: myLatLng,  map: map, icon: \'images/mm_20_green.png\' });       	
	        google.maps.event.addListener(map, \'click\', 
	          function( event){
	            alert("OK memorizzata coordinata \n lat="+event.latLng.lat()+" \n long="+event.latLng.lng()+" \n\n Ora puoi usare funzione \'Pesca Coordinata dalla mappa\' se presente");
	            document.getElementById(\'latitudine\').innerHTML=event.latLng.lat();
	            document.getElementById(\'longitudine\').innerHTML=event.latLng.lng();
	          }
	        );  
	    		
	    		
	      } else {
	    	}
			            
	    }
	    
	    function Mostra_Foto_Ajax(progressivo, sequenza, id_foto, flag_foto, pdr){
				   var xhr = new XMLHttpRequest();
				   xhr.onreadystatechange = function(){
				      if(xhr.readyState == 4 && xhr.status == 200) {
				         var v=xhr.responseText.split("|");
				         //alert(v);
				         if (v[0]==1){
				         	
				         	 var larghezza=parseInt(v[2]);
				         	 var altezza=parseInt(v[3]);
				         	 var direzione=v[4];
				         	 var proporzione=parseFloat(v[5]);
				         	 var lato_lungo='.($lato_foto-8).';
				         	 var lato_corto;
				         	 var dimensioni;
				         	 
				         	 if (direzione=="ver"){proporzione=1/proporzione;}
				         	 lato_corto=Math.round(lato_lungo*proporzione);
				         	 
				         	 if (direzione=="ver"){dimensioni=\' width="\'+lato_corto+\'" height="\'+lato_lungo+\'" \';}
				         	 if (direzione=="orr"){dimensioni=\' width="\'+lato_lungo+\'" height="\'+lato_corto+\'" \';}
				         	 
				         	 var image_x = document.getElementById("img_foto_"+id_foto);
								   image_x.parentNode.removeChild(image_x);
								   v[1]+="#" + new Date().getTime();
				         	 document.getElementById("foto"+id_foto).innerHTML = \'<img src="\'+v[1]+\'" data-zoom-image="\'+v[1]+\'" id="img_foto_\'+id_foto+\'" border="0" \'+dimensioni+\' class="foto-contatore" />\';
				         	 $("#img_foto_"+id_foto).elevateZoom({zoomType:"inner",cursor:"crosshair",zoomWindowFadeIn:500,zoomWindowFadeOut:750}); 
				         	 
				         	 // Bottone Download ON
				         	 document.getElementById("dwnld_foto"+id_foto).innerHTML = \'<a href="\'+v[1]+\'" download="\'+pdr+\'_\'+id_foto+\'.jpg" title="ImageName"><img src="images/dwnld_foto_on.png" border="0" alt="[ ]" title="download foto"></a>\';

				        }  else {
				         	 var image_x = document.getElementById("img_foto_"+id_foto);
								   image_x.parentNode.removeChild(image_x);
				         	 document.getElementById("foto"+id_foto).innerHTML = \'<img src="images/nofoto.png" data-zoom-image="images/nofoto.png" id="img_foto_\'+id_foto+\'" border="0" \'+dimensioni+\' class="foto-contatore"  />\';
				         	 $("#img_foto_"+id_foto).elevateZoom({zoomType:"inner",cursor:"crosshair",zoomWindowFadeIn:500,zoomWindowFadeOut:750}); 
				         	 
				         	 // Bottone Download OFF
				         	 document.getElementById("dwnld_foto"+id_foto).innerHTML = \'<img src="images/dwnld_foto_off.png" border="0" alt="[ ]" title="download foto disabilitato">\';
				         }
				      }
				   }
	     	  document.getElementById("foto"+id_foto).innerHTML = \'<img src="images/loader.gif" id="img_foto_\'+id_foto+\'" border="0" class="foto-contatore" style="width:190px; height:125px;" />\';
				  url="get_foto_ajax.php?progressivo="+progressivo+"&sequenza="+sequenza+"&id_foto="+id_foto+"&azienda='.$UtenteDitta.'&sede='.$UtenteSede.'";
				  // alert(url);
				   xhr.open("GET", url, true);
				   xhr.send();
	    }
	    
	    function Pausa(){setTimeout(myFunction, 500);}
	    
			
			$(function(){
      	  var pDettagli = $("#pDettagli").panel();
			
					pDettagli.panel({
						  title:"Dettagli",
						  iconCls:"icon-search",
					    width:"100%",
					    //height:"'.($lato_foto+$altezza_dato+45).'",
							collapsible:true
					}); 
			});
			
			
	
			
			function isNumeric(n) {
			  return !isNaN(parseFloat(n)) && isFinite(n);
			}
			
			$("#foto_1_rot_1").click(function(){RuotaFoto(1, -90); return false; });
			$("#foto_1_rot_2").click(function(){RuotaFoto(1, 90); return false; });
			$("#foto_1_rot_3").click(function(){RuotaFoto(1, 180); return false; });
			
			$("#foto_2_rot_1").click(function(){RuotaFoto(2, -90); return false; });
			$("#foto_2_rot_2").click(function(){RuotaFoto(2, 90); return false; });
			$("#foto_2_rot_3").click(function(){RuotaFoto(2, 180); return false; });
			
			$("#foto_3_rot_1").click(function(){RuotaFoto(3, -90); return false; });
			$("#foto_3_rot_2").click(function(){RuotaFoto(3, 90); return false; });
			$("#foto_3_rot_3").click(function(){RuotaFoto(3, 180); return false; });
			
			function RuotaFoto(id_foto, gradi){
        /*var image_x = document.getElementById("img_foto_"+id_foto);
        var url=image_x.src;
        var xhr = new XMLHttpRequest();
				   xhr.onreadystatechange = function(){
				      if(xhr.readyState == 4 && xhr.status == 200) {
				         	
				         	EliminaFotoEZoom();
				         	
				         	//indice_barra=url.lastIndexOf("/");
				         	//nomefoto=url.substr((indice_barra+1)-url.length);
				         	//v=nomefoto.split("_");
				         	//progressivo=v[0];
				         	//sequenza=v[1];
				         	
				         	//alert("Prog="+progressivo+" seq="+sequenza+" flagfoto1="+flag_foto_1);
				         	
						     	Mostra_Foto_Ajax(progressivo,sequenza,1,flag_foto_1);
						     	Mostra_Foto_Ajax(progressivo,sequenza,2,flag_foto_2);
						     	Mostra_Foto_Ajax(progressivo,sequenza,3,flag_foto_3);
				      }
				   }
			  xhr.open("GET", "ruota_foto.php?url="+url+"&gradi="+gradi, true);
			  xhr.send();*/
			  
			  $("#img_foto_1").rotate(gradi);
			  
			}
			
			
			function ResettaDettagli(){
				EliminaFotoEZoom();
				document.getElementById("lista_etichette").innerHTML=lista_etichette
	     	document.getElementById("lista_dati").innerHTML=lista_dati
			}
			
			
			
		</script>
		
	             
		
	<!-- Easy UI -->	
	
	<style type="text/css"> 
	
		/* Stili per il layout fluido */
		html,body{margin: 0;padding:0;}
		body{background-color: #fff; color: #000; }
		
		/* Contenitore sito, � quello che ha immagine di sfondo fissa del cielo di notte */
		div#contenitore{font-family: verdana,arial,sans-serif;font-size: 76%; background:url(images/top_img_sito_gea_4.jpg) repeat-x; }
		
		/* Il Sito comprende le principali parti Head-Body_Foot */
		div#sito{margin: 0 auto;text-align: left; }
		div#corpo_sito{background-color: #fff; color: #000;}
		 
		/*stili generici, su header e footer*/
		
		h1,h2{margin: 0;padding:0}
		h1{padding-left:0.5em;font: bold 2.3em/80px arial,serif}
		h2{color: #999;font-size: 1.5em}
		
		div#header{height:60px;}
		div#filtri{height:230px; background-color: #d2d5dc;}
		          
	 div#divisorio{
		           height:1px; 
		          }
		
		 
		/*stili per la navigazione nel menu principale*/
		div#navigation ul{margin: 0;padding: 0; position: absolute; top: 36px; left: 100px; text-align:left; list-style-type: none; line-height: 2em; }
		div#navigation li{display: inline-block; margin: 0 0 0 0em;padding: 0; } 
		div#navigation a{display: block; padding: 0 0.5em; color:#fff;font: normal bold; verdana, arial,sans-serif; font-weight: bold; text-decoration: none; }
		div#navigation a:hover{color: #fff; background-color: #74809e;}
		div#navigation a#activelink{color: #384a76; background-color: #fff;}

	  div#content{
	              margin:0 0px; 
	              padding:1em 0px; 
	             }
		
		
		div#footer{
		           height: 10px;
		           text-align:center;
		           padding:0.5em; 
		           background-color: #fff;
	             color: #909090;
		           clear:both;
		           font-family: verdana,arial,sans-serif;
		           font-size: 10px;
		           text-decoration: none;
		          }
		div#footer a{
		             color: #606060;
		             font-weight: bold;
		             text-decoration: underline;
		            
		          }
		          
	  
		            
		div.flash{margin-bottom: 10px; margin-top: 10px; align:center;}	
	
		
		a.email:link{color:#FFFFFF; text-decoration:underline; font-family:Verdana, sans-serif, Arial, Helvetica; font-size:10px;}
		a.email:visited{color:#FFFFFF; text-decoration:underline; font-family:Verdana, sans-serif, Arial, Helvetica; font-size:10px;}
		a.email:hover{color:#FFFFFF; text-decoration:underline; font-family:Verdana, sans-serif, Arial, Helvetica; font-size:10px;}
		a.email:active{color:#FFFFFF; text-decoration:underline; font-family:Verdana, sans-serif, Arial, Helvetica; font-size:10px;}
		
		.dettagli-container{min-width:970px; 
		                    margin:5px 5px;
		                    position:relative;}
		
		.foto_container{position:relative; 
		                width:'.$lato_foto.'px; 
		                height:'.$lato_foto.'px; 
		                float:left; 
		                margin:0 '.$margin_foto.'px; }
		                
		div.div-foto-contatore{width:'.$lato_foto.'px; 
		                       height:'.$lato_foto.'px; 
		                       line-height:'.$lato_foto.'px;
		                       text-align:center; 
		                       border-radius: 10px; 
		                       background-color:#ccd9e9;
		                       }
		                      
		
	 .foto-contatore{vertical-align:middle;}
	 .foto_label{position:absolute;
		            left:'.(($lato_foto-$larghezza_label)/2).'px;
	              top:0px; 
	              width:'.$larghezza_label.'px;
	              height:30px;
	              background-color:#ccd9e9;
	              z-index:1000; 
	              border-radius: 0px 0px 8px 8px;
	              box-shadow: 0px 3px 1px rgba(0,0,0,0.3);
	              text-align: center;
							  vertical-align: middle;
							  line-height: 30px;}
							  
							  
	.div-dettagli-dati{margin:'.$margin_foto.'px;
	                   padding:3px 5px; 
                     clear:left;
                     position:absolute;
                     top:'.($lato_foto+31).'px;
                     width: '.$larghezza_dati.'px; 
                     height:'.$altezza_dato.'px; 
                     text-align:center;  
                     border-radius: 10px; 
                     background-color:#ccd9e9;
                     border: '.$margin_foto.'px solid #aabbd0;}     						  
							  
							  
	
	 
  .testo-label{
								    font-family:Verdana, Arial, Helvetica, sans-serif; 
									    font-size:10px;
									        color:#606060; 
								} 	                
	.testo-dato{
								    font-family:Verdana, Arial, Helvetica, sans-serif; 
									    font-size:10px;
									        color:#303030; 
								} 	                							
  .dettagli-label{width:100px; text-align:right; float:left; margin-top:20px;}
	.dettagli-dato{margin-left:5px; text-align:left; font-weight:bold; float:left; margin-top:20px;}                  
	
	.div-mappa-contatore{margin-top: 0px;
		                   margin-botton: 0px;
		                   margin-left: '.($larghezza_dati+15).'px;
		                   margin-right: 1px;
	                     width:auto;
	                     height:'.$altezza_mappa.'px;
	                     text-align:center; 
	                     border-radius: 10px; 
		                   background-color:#ccd9e9;
		                   border: '.$margin_foto.'px solid #aabbd0; 
	  	                } 
	                    
	
			                                                        
		                  
		
								 
		
		
		//.foto1_label{left:95px;}						 
		//.foto2_label{left:493px;}						 
		//.foto3_label{left:891px;}		
		
		.rot_1{position:absolute; top:5px; left:4px; }				 
		.rot_2{position:absolute; top:5px; right:4px; }				 
		.rot_3{position:absolute; bottom:1px; right:7px; }				 
	  
	  .dwnld_foto{position:absolute; bottom:2px; left:7px; }
	
	</style>
	
	    
	
		
	<script language="JavaScript1.2">
	
		
		function finestra(nome) {
		 var attributi;
		 attributi = "top=0, left=0";
		 attributi += ",alwaysRaised=yes";
		 attributi += ",toolbar=no";
		 attributi += ",location=no";
		 attributi += ",directories=no";
		 attributi += ",status=no";
		 attributi += ",menubars=no";
		 attributi += ",scrollbars=no";
		 attributi += ",resizable=no";
		 attributi += ",copyhistory=no";
		 window.open(nome, "subwindow", attributi);
	  }
	  
  </script>
  

  
  
  
	
</head>	
';

?>
