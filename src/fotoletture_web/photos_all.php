<?php

ini_set('memory_limit', '-1');

// Compatibility function for PHP < 5.4
if (!function_exists("http_response_code")) {
    function http_response_code($newcode = NULL) {
        static $code = 200;
        if ($newcode !== NULL) {
            header("X-PHP-Response-Code: ".$newcode, true, $newcode);
            if (!headers_sent()) {
                $code = $newcode;
            }
        }
        return $code;
    }
}

set_time_limit(0);
if (!isset($_GET["sede_id"]) || !isset($_GET["progressivo"]) || !isset($_GET["numero_invio"]) || !isset($_GET["indice_file"])) {
    http_response_code(500);
    exit("Required paramters sede_id, progressivo, numero_invio and indice_file not set");
}

require_once("config/config_foto.php");

Apri_DB("system");

$row = executeQuery("SELECT sede_ditta, sede_nome FROM sede WHERE sede_id = ".$_GET["sede_id"]);
$companyName = $row["sede_ditta"];
$locationName = $row["sede_nome"];

//echo "Ditta: ".$companyName."  Sede: ".$locationName."<br>\n";

// Purtroppo posso inizializzare questa variabile soltanto dalla tabella letture, quindi lo faccio dopo
$config = null;

//Apri_DB($_GET["sede_id"]);
Apri_DB("sede");


$lettureQuery = "SELECT id
                      , progressivo
                      , sequenza
                      , flag_foto_1
                      , flag_foto_2
                      , flag_foto_3
                      , flag_foto_4
                      , flag_foto_5
                      , flag_foto_6
                      , codice_ente
                   FROM letture
                  WHERE progressivo = ".$_GET['progressivo']."
                    AND numero_invio = ".$_GET['numero_invio'];

if (isset($_GET["nomefilezip"])) {
    $nomefilezip = $_GET["nomefilezip"];
} else {
    $nomefilezip = "";
}

$indice_file = 0;
if (isset($_GET["indice_file"])) {
    $lettureQuery .= " AND indice_file = ".$_GET["indice_file"];
    $indice_file = $_GET["indice_file"];
}

if (isset($_GET["limit"]) && isset($_GET["offset"])) {
    $lettureQuery .= " LIMIT ".$_GET["limit"]."
                      OFFSET ".$_GET["offset"];
}

$lettureQueryRes = mysql_query($lettureQuery);
if (!$lettureQueryRes) {
    http_response_code(500);
    exit("Errore durante l'esecuzione della query.\n\n".mysql_error());
}

//$zipPath = isset($_GET["path"]) ? $_GET["path"] : "/gea_4_sologas/api/zip/archivi/".uniqid().".zip";
//$zipPath = isset($_GET["path"]) ? $_GET["path"] : "archivi/".uniqid().".zip";
$zipPath = "archivi/".uniqid().".zip";

//$zipPath = "../../../files_compressi/".$zipPath2;

//echo "provo a creare zip: ".$zipPath."<br>\n";

$zip = new ZipArchive();

//echo "verifico zip: ".$zipPath."<br>\n";

if (file_exists($zipPath)) {
    if ($zip->open($zipPath) !== true) {
        //echo "Impossibile creare o aprire il file zip";
        http_response_code(500);
        exit("Impossibile creare o aprire il file zip.");
    }
} else {
//    echo "percorso zip creato: ".$zipPath.">br>";
    if ($zip->open($zipPath, ZipArchive::CREATE) !== true) {
        //echo "Impossibile creare o aprire il file zip";
        http_response_code(500);
        exit("Impossibile creare o aprire il file zip.");
    }
}

//die($zipPath);


$counter = 0;
$contatore_foto = 0;
$errors = array();
$foto = array();
while ($row = mysql_fetch_assoc($lettureQueryRes)) {

    // Al primo giro, carico la configurazione
    if ($config == null) {
        //Apri_DB(0);
        Apri_DB("system");
        //echo "database=".$database;
        $configRow = executeQuery("SELECT config_foto_xml
                                     FROM ente
                                    WHERE ente_id = ".$row["codice_ente"]);

        $config = simplexml_load_string($configRow["config_foto_xml"]);

        if (!$config) {
            http_response_code(500);
            echo "Errore durante il caricamento della configurazione XML: ";
            foreach(libxml_get_errors() as $error) {
                echo "<br>", $error->message;
            }
            exit();
        }
        //Apri_DB($_GET["sede_id"]);
        Apri_DB("sede");
    }

    //die($companyName." ".$locationName." ");


    if (addAllFileToZip($companyName, $locationName, $row, $zip, $config)) {
        $counter++;
    } else {
        array_push($errors, "Impossibile aggiungere foto della lettura con sequenza ".$row["sequenza"].".");
    }

}

array_push($foto, "File zip creato con successo. Numero totale di fotografie inserite: ".$contatore_foto);


$zip->close();

if (file_exists("archivi/".$nomefilezip)){}

//echo "esco subito<br>";
//die();


//echo "fase 1<br>";

$iterator = $config->children();

//echo "fase 2<br>";


$zipNode = $iterator[0];
//echo "fase 3<br>";

$zipNodeAttributes = $zipNode->attributes();
//echo "fase 4<br>";

if ($nomefilezip === "") {
    $zipDownloadName = $zipNodeAttributes["Name"] != null ? $zipNodeAttributes["Name"] : "photos.zip";
    } else {
    //$zipDownloadName = "/gea_4_sologas/api/zip/archivi/".$nomefilezip;
    $zipDownloadName = "archivi/".$nomefilezip;
    }

//echo "fase 5<br>";


/*header("Content-type: application/zip");
header("Content-Length: ".filesize($zipPath));
header("Content-Disposition: attachment; filename=".($zipNodeAttributes["Name"] != null ? $zipNodeAttributes["Name"] : "photos.zip"));
readfile($zipPath);*/

//echo "fase 6<br>";

/*if (filter_var($_GET['browser_mode'], FILTER_VALIDATE_BOOLEAN)) {

    //echo "fase 7<br>";
    //header('Content-type: text/html');

    echo "<!DOCTYPE html>
        <html>
            <head>
                <title>Creazione zip</title>
            </head>

            <body>
                <p>File zip creato con successo. Numero totale di fotografie inserite: ".$contatore_foto."</p>
                <p><a href='download.php?path=".$zipPath."&name=".$zipDownloadName."'>Clicca qui</a> per procedere al download.</p>";
    if (isset($_GET["limit"]) && isset($_GET["offset"])) {
        echo "<p><a href='photos.php?path=".$zipPath."&sede_id=".$_GET["sede_id"]."&progressivo=".$_GET["progressivo"]."&numero_invio=".$_GET["numero_invio"]."&indice_file=".$_GET["indice_file"]."&offset=".($_GET["offset"] + $_GET["limit"])."&limit=".$_GET["limit"]."'>Clicca qui</a> per aggiungere le foto di altre ".$_GET["limit"]." letture allo zip.";
    }

    foreach ($errors as $error) {
        echo "<p>.$error.</p>";
    }
    echo "    </body>
        </html>";

 }else {
*/
    //echo "fase 8<br>";
    //echo "RENAME<br>";
    //echo $zipPath."<br>";
    //echo $zipDownloadName."<br>";
    rename($zipPath, $zipDownloadName);
    if ($indice_file==0)
        {$uploaddir = "../GEA_4/foto/".$companyName."_".strtolower($locationName)."/restituzioni/FOTO";}
        // {$uploaddir = "../../../SMAT_2017_RESTITUZIONE_DATI_SOLOGAS/FOTO";}
        //{$uploaddir = "F:/SMAT_2017_RESTITUZIONE_DATI_SOLOGAS/FOTO";}
    else
        {$uploaddir = "../GEA_4/foto/".$companyName."_".strtolower($locationName)."/restituzioni/FOTO/".$indice_file;}
        //{$uploaddir = "../../../SMAT_2017_RESTITUZIONE_DATI_SOLOGAS/FOTO/".$indice_file;}
        //{$uploaddir = "F:/SMAT_2017_RESTITUZIONE_DATI_SOLOGAS/FOTO/".$indice_file;}
        //echo $uploaddir;
    if (!(file_exists($uploaddir))){if (mkdir($uploaddir,0755)){echo "cartella creata: ".$uploaddir."<br>";}else{echo "errore creazione cartella: ".$uploaddir."<br>";}}
    $uploadfile = $uploaddir."/".$nomefilezip;
    //echo $zipDownloadName."<br>";
    //echo $uploadfile."<br>";
    //echo $uploadfile . " -" .$_FILES['userfile']['tmp_name'];

  if(copy($zipDownloadName, "../GEA_4/foto/".$companyName."_".strtolower($locationName)."/restituzioni/FOTO/".$indice_file."/".$nomefilezip)){
    //if(copy($zipDownloadName, "F:/SMAT_2017_RESTITUZIONE_DATI_SOLOGAS/FOTO/".$indice_file."/".$nomefilezip)){
       //echo "OK";
    } else {
         //echo "KO";
    }

    // Commento la generazione dell xml perch? voglio rilevare direttamente gli errori da gea4
    /*
    $xml = new SimpleXMLElement('<xml/>');
    $xml->addChild("Path", $zipPath);
    if (count($errors) > 0) {
        $errorsNode = $xml->addChild("Errors");
        foreach ($errors as $error) {
            $errorsNode->addChild("Error", $error);
        }
    }
    //header("Content-type: text/xml");
    echo $xml->asXML();
    */
    foreach ($foto as $fotobuff) {
        echo $fotobuff."<br>";
    }

    foreach ($errors as $error) {
        echo $error."<br>";
    }

//}


//echo "fase 9<br>";


function executeQuery($query) {
    $res = mysql_query($query);
    if (!$res) {
        http_response_code(500);
        exit("Errore durante l'esecuzione della query.\n\n".mysql_error());
    }
    return mysql_fetch_assoc($res);
}

function esisteFile($companyName, $locationName, $row, $photoNumber){
    global $foto;

    $progressive = str_pad($row["progressivo"], 6, "0", STR_PAD_LEFT);
    $sequence = str_pad($row["sequenza"], 6, "0", STR_PAD_LEFT);

    $query = "../GEA_4/foto/".$companyName."_".strtolower($locationName)."/"
           ."lav_".$progressive."/"
           .substr($sequence, 0, 1)."00000/"
           .substr($sequence, 1, 1)."0000/"
           .substr($sequence, 2, 1)."000/"
           .substr($sequence, 3, 1)."00/"
           .$progressive."_".$sequence."_".$photoNumber."*.jpg";

    $latestFile = null;
    $latestFileTimestamp = null;

    foreach(glob($query) as $file) {
        $timestamp = date("Y-m-d H:i:s", filemtime($file));
        //$timestamp = strtotime(substr($file, 0, 4)."-".substr($file, 4, 2)."-".substr($file, 6, 2)." ".substr($file, 9, 2).":".substr($file, 11, 2).":".substr($file, 13, 2));

        if ($latestFileTimestamp == null || $latestFileTimestamp < $timestamp) {
            $latestFile = $file;
            $latestFileTimestamp = $timestamp;
        }
    }

    if ($latestFile == null)  {
        array_push($foto, "Foto non trovata SEQ=".$sequence." ID=".$photoNumber);
        //echo "Foto non trovata SEQ=".$sequence." ID=".$photoNumber."<br>";
        //echo $query."<br>";
        return "";

    } else {
        array_push($foto, "Foto trovata=".$latestFile);
    //echo "Foto trovata=".$latestFile."<br>";
        return $latestFile;
    }

}

function esisteFileRipasso($companyName, $locationName, $row, $photoNumber, $nomefoto){
    global $foto;

    $progressive = str_pad($row["progressivo"], 6, "0", STR_PAD_LEFT);
    $sequence = str_pad($row["sequenza"], 6, "0", STR_PAD_LEFT);

    $query = "../GEA_4/foto/".$companyName."_".strtolower($locationName)."/"
           ."lav_".$progressive."/"
           .substr($sequence, 0, 1)."00000/"
           .substr($sequence, 1, 1)."0000/"
           .substr($sequence, 2, 1)."000/"
           .substr($sequence, 3, 1)."00/"
           .$nomefoto;

    $latestFile = null;
    $latestFileTimestamp = null;

    foreach(glob($query) as $file) {
        $timestamp = date("Y-m-d H:i:s", filemtime($file));
        //$timestamp = strtotime(substr($file, 0, 4)."-".substr($file, 4, 2)."-".substr($file, 6, 2)." ".substr($file, 9, 2).":".substr($file, 11, 2).":".substr($file, 13, 2));

        //if ($latestFileTimestamp == null || $latestFileTimestamp < $timestamp) {
        //echo $file;
            $latestFile = $file;
        //    $latestFileTimestamp = $timestamp;
        //}
    }

    if ($latestFile == null)  {
        array_push($foto, "Foto non trovata SEQ=".$sequence." ID=".$photoNumber);
        //echo "Foto non trovata SEQ=".$sequence." ID=".$photoNumber."<br>";
        //echo $query."<br>";
        return "";

    } else {
        array_push($foto, "Foto trovata=".$latestFile);
    //echo "Foto trovata=".$latestFile."<br>";
        return $latestFile;
    }

}


function addFileToZip($companyName, $locationName, $row, $zip, $config, $photoNumber) {
    global $contatore_foto;
    global $foto;

    // contatore foto
    //echo "addfiletozip file".$file."<br>";
    //echo "contatorefoto".$contatore_foto."<br>";

    $fileFoto = "";
    $fileFoto = esisteFile($companyName, $locationName, $row, $photoNumber);

    if ($fileFoto!==""){
        $contatore_foto++;
    //echo "contatorefoto dopo".$contatore_foto."<br>";
        $customFileName = getFileNameFromConfig($config, $row["id"], $contatore_foto);
    if (!$customFileName) {
                            return false;
                              } else {
                        if ($zip->addFile($fileFoto, $customFileName)===TRUE){
                                                    array_push($foto, "Foto trovata=".$fileFoto);
                                                    array_push($foto, "Foto ente=".$customFileName);
                                                    //echo "aggiungo file ".$customFileName."<br>";
                                                    //echo "Foto ente=".$customFileName."<br>";
                                                    return true;
                                                    } else {
                                                    return false;
                                                        }
                             }
        } else {
        return false;
        }
}

function addAllFileToZip($companyName, $locationName, $row, $zip, $config) {
    global $contatore_foto;
    global $foto;

    $progressive = str_pad($row["progressivo"], 6, "0", STR_PAD_LEFT);
    $sequence = str_pad($row["sequenza"], 6, "0", STR_PAD_LEFT);

    $query = "../GEA_4/foto/".$companyName."_".strtolower($locationName)."/"
           ."lav_".$progressive."/"
           .substr($sequence, 0, 1)."00000/"
           .substr($sequence, 1, 1)."0000/"
           .substr($sequence, 2, 1)."000/"
           .substr($sequence, 3, 1)."00/"
           .$progressive."_".$sequence."_*.jpg";

    //die("AddAllFileToZip: ".$query);

    $progressivo_foto = 0;
    foreach(glob($query) as $file) {

        if ($file == '..' || $file == '.' ){

            //echo "cartella";

            }
        else{
            $contatore_foto++;
            $progressivo_foto++;
            $customFileName = getFileNameFromConfig($config, $row["id"], $progressivo_foto);
            //die("customefileName: ".$customFileName);
            if ($customFileName=="") {
                                    //die("errore a leggere il nome da config");
                                    return false;
                                    } else {
                                    if ($zip->addFile($file, $customFileName)===TRUE){
                                    array_push($foto, "Foto ente=".$customFileName);
                                                    //echo "aggiungo file ".$customFileName."<br>";
                                                    //echo "Foto ente=".$customFileName."<br>";
                                                    // return true;
                                                    } else {
                                                    return false;
                                                        }
                                        }
            }
    }
    return true;

}

function addFileRipassoToZip($companyName, $locationName, $row, $zip, $config, $photoNumber, $nomefoto) {
    global $contatore_foto;
    global $foto;

    // contatore foto
    //echo "addfiletozip file".$file."<br>";
    //echo "contatorefoto".$contatore_foto."<br>";

    $fileFoto = "";
    $fileFoto = esisteFileRipasso($companyName, $locationName, $row, $photoNumber, $nomefoto);

    if ($fileFoto!==""){
        $contatore_foto++;
    //echo "contatorefoto dopo".$contatore_foto."<br>";
        $customFileName = getFileNameFromConfig($config, $row["id"], $contatore_foto);
    if (!$customFileName) {
                            return false;
                              } else {
                        if ($zip->addFile($fileFoto, $customFileName)===TRUE){
                        array_push($foto, "Foto ente=".$customFileName);
                                                    //echo "aggiungo file ".$customFileName."<br>";
                                                    //echo "Foto ente=".$customFileName."<br>";
                                                    return true;
                                                    } else {
                                                    return false;
                                                        }
                             }
        } else {
        return false;
        }
}


function getFileNameFromConfig($config, $letturaId, $photoNumber) {
    $query = "SELECT ";

    // Brutto, ma compatibile con qualsiasi versione di PHP
    $iterator = $config->children();
    $zipNode = $iterator[0];
    $iterator = $zipNode->children();
    $fieldsNode = $iterator[0];
    $fieldsNodes = $fieldsNode->children();

    $index = 0;
    foreach($fieldsNodes as $field) {
        $attributes = $field->attributes();
        $query .= $index == 0 ? $attributes["DbName"] : ", ".$attributes["DbName"];
        $index++;
    }
    //echo $query."<br>";
    //echo "id lettura: ".$letturaId."<br>";
    $row = executeQuery($query." FROM letture WHERE id = ".$letturaId);


    $index = 0;
    $result = "";
    $fieldsNodeAttributes = $fieldsNode->attributes();
    foreach(str_split($fieldsNodeAttributes["Format"]) as $char) {
        switch ($char) {
            case "%":
                $fieldNode = $fieldsNodes[$index];
                $fieldNodeAttributes = $fieldNode->attributes();
                $iterator = $fieldNode->children();
                $rulesNode = $iterator[0];

                $value = $row[(string) $fieldNodeAttributes["DbName"]];
                //echo "pdr:".$value."<br>";

                if ($rulesNode != null && $rulesNode->children() != null) {
                    switch ($fieldNodeAttributes["Type"]) {
                        case "String":
                            foreach ($rulesNode->children() as $ruleNode) {
                                $ruleNodeAttributes = $ruleNode->attributes();

                                switch ($ruleNodeAttributes["Type"]) {
                                    case "Lowercase":
                                        $value = strtolower($value);
                                        break;

                                    case "Pad":
                                        //echo "pdr:".$value."<br>";
                                        //echo "lunghezza:".$ruleNodeAttributes["Length"]."<br>";
                                        //echo "carattere:".$ruleNodeAttributes["Character"]."<br>";
                                        //echo "direzione:".$ruleNodeAttributes["Direction"]."<br>";
                                        if ($ruleNodeAttributes["Direction"] === "Left") {
                                            $tipopad = "STR_PAD_LEFT";
                                            $riempimento = str_repeat($ruleNodeAttributes["Character"],(int)$ruleNodeAttributes["Length"]);
                                            //echo "riempimento: ".$riempimento;
                                            $value = $value.$riempimento;
                                            $value = substr($value, 0, (int)$ruleNodeAttributes["Length"]);
                                            } {
                                            $tipopad = "STR_PAD_RIGHT";
                                            $riempimento = str_repeat($ruleNodeAttributes["Character"],(int)$ruleNodeAttributes["Length"]);
                                            //echo "riempimento: ".$riempimento;
                                            $value = $riempimento.$value;
                                            $value = substr($value, -(int)$ruleNodeAttributes["Length"]);
                                            }

                                        //echo "prima valore pad: ".$value."<br>";
                                        //$value = str_pad(
                                        //    $value,
                                        //    $ruleNodeAttributes["Length"],
                                        //    $ruleNodeAttributes["Character"],
                                        //    $tipopad);

                                        //echo "dopo valore pad: ".$value."<br>";

                                        break;

                                    case "Replace":
                                        $iterator = $ruleNode->children();
                                        $replacementsNode = $iterator[0];
                                        foreach ($replacementsNode as $replacementNode) {
                                            $replacementNodeAttributes = $replacementNode->attributes();
                                            $value = str_replace($replacementNodeAttributes["OldCharacter"], $replacementNodeAttributes["NewCharacter"], $value);
                                        }
                                        break;

                                    case "Substring":
                                        //echo "pdr before substring:".$value."<br>";
                                        $value = substr($value, (int)$ruleNodeAttributes["Start"], (int)$ruleNodeAttributes["Length"]);
                                        //echo "pdr after substring:".$value."<br>";
                                        break;

                                    case "Uppercase":
                                        $value = strtoupper($value);
                                        break;

                                    default:
                                        http_response_code(500);
                                        exit("Tipo di regola '".$ruleNodeAttributes["Type"]."' non riconosciuta");
                                }
                            }

                            break;

                        case "Date":
                            $timestamp = strtotime(substr($value, 6, 4)."-".substr($value, 3, 2)."-".substr($value, 0, 2)." 00:00:00");
                            foreach ($rulesNode->children() as $ruleNode) {
                                $ruleNodeAttributes = $ruleNode->attributes();

                                switch ($ruleNodeAttributes["Type"]) {
                                    case "Format":
                                        $value = date($ruleNodeAttributes["Format"], $timestamp);
                                        break;

                                    default:
                                        http_response_code(500);
                                        exit("Tipo di regola '".$ruleNodeAttributes["Type"]."' non riconosciuta");
                                }
                            }
                            break;

                        case "Time":
                            $timestamp = "1970-01-01 ".$value;
                            foreach ($rulesNode->children() as $ruleNode) {
                                $ruleNodeAttributes = $ruleNode->attributes();

                                switch ($ruleNodeAttributes["Type"]) {
                                    case "Format":
                                        $value = date($ruleNodeAttributes["Format"], $timestamp);
                                        break;

                                    default:
                                        http_response_code(500);
                                        exit("Tipo di regola '".$ruleNodeAttributes["Type"]."' non riconosciuta");
                                }
                            }
                            break;

                        default:
                            http_response_code(500);
                            exit("Tipo di campo '".$fieldNodeAttributes["Type"]."' non riconosciuto");
                    }
                }

                $result .= $value;
                $index++;
                break;

            case "#":
                $result .= str_pad($photoNumber,3,"0",STR_PAD_LEFT);
                break;

            default:
                $result .= $char;
                break;
        }
    }

    return $result;
}

/* creates a compressed zip file */
function create_zip($files = array(),$destination = '',$overwrite = false) {
    //if the zip file already exists and overwrite is false, return false
    if(file_exists($destination) && !$overwrite) { return false; }
    //vars
    $valid_files = array();
    //if files were passed in...
    if(is_array($files)) {
        //cycle through each file
        foreach($files as $file) {
            //make sure the file exists
            if(file_exists($file)) {
                $valid_files[] = $file;
            }
        }
    }
    //if we have good files...
    if(count($valid_files)) {
        //create the archive
        $zip = new ZipArchive();
        if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
            return false;
        }
        //add the files
        foreach($valid_files as $file) {
            $zip->addFile($file,$file);
        }
        //debug
        //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

        //close the zip -- done!
        $zip->close();

        //check to make sure the file exists
        return file_exists($destination);
    }
    else
    {
        return false;
    }
}

?>